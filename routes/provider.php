<?php

use App\Http\Controllers\Provider\Advert\AdvertController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/provider/dashboard');
});

// Login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Register
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Reset Password
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Confirm Password
Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');

// Verify Email
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::middleware('provider.verified')->group(function () {

    // Dashboard
    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::prefix('profile')->name('profile')->namespace('Profile')->group(function () {

        Route::get('/{id}', 'ProviderProfileController@show')->name('.show');

        Route::get('/edit/{id}', 'ProviderProfileController@edit')->name('.edit');

        Route::post('/update/{id}', 'ProviderProfileController@update')->name('.update');
    });

    Route::prefix('albums')->name('albums')->namespace('Album')->group(function () {

        Route::get('/', 'AlbumController@index')->name('.index');

        Route::post('/store', 'AlbumController@store')->name('.store');

        Route::get('/{id}', 'AlbumController@show')->name('.show');

        Route::post('/{id}', 'AlbumController@update')->name('.update');
    });

    Route::prefix('/sounds')->name('sounds')->namespace('Sound')->group(function () {

        Route::post('/store', 'SoundController@store')->name('.store');

        Route::get('/{id}', 'SoundController@show')->name('.show');

        Route::post('/{id}', 'SoundController@update')->name('.update');
    });

    Route::prefix('/adverts')->name('adverts')->namespace('Advert')->group(function(){

        Route::get('/', 'AdvertController@create')->name('.create');

        Route::post('/', 'AdvertController@store')->name('.store');

        Route::get('/running', 'AdvertController@running')->name('.running');

        Route::get('/history', 'AdvertController@index')->name('.index');

    });

    Route::prefix('/transactions')->name('transactions')->namespace('Transaction')->group(function(){

        Route::get('/verify', 'TransactionController@verify')->name('.verify');

        Route::get('/', 'TransactionController@index')->name('.index');
    });

    Route::prefix('payment-methods')->name('payment-methods')->namespace('PaymentMethod')->group(function() {

        Route::get('/', 'PaymentMethodController@index')->name('.index');
        Route::post('/store', 'PaymentMethodController@store')->name('.store');
    });
});
