<?php

use Illuminate\Support\Facades\Route;

// Dashboard
Route::get('/', 'HomeController@index')->name('home');

// Login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Register
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Reset Password
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Confirm Password
Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');

// Verify Email
// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
// Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
// Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::prefix('categories')->namespace('Categories')->name('categories')->group(function () {
    Route::get('/', 'AdminCategoryController@index')->name('.index');
    Route::post('/store', 'AdminCategoryController@store')->name('.store');
    Route::post('/update/{id}', 'AdminCategoryController@update')->name('.update');
});

Route::prefix('providers')->namespace('Providers')->name('providers')->group(function () {
    Route::get('/', 'AdminProviderController@index')->name('.index');
    Route::get('/create', 'AdminProviderController@create')->name('.create');
    Route::post('/store', 'AdminProviderController@store')->name('.store');
    Route::get('/show/{id}', 'AdminProviderController@show')->name('.show');

    Route::prefix('albums')->namespace('Albums')->name('.albums')->group(function () {
        Route::get('/', 'AdminProviderAlbumController@index')->name('.index');
        Route::get('/{id}', 'AdminProviderAlbumController@show')->name('.show');
    });

    Route::prefix('sounds')->namespace('Sounds')->name('.sounds')->group(function () {
        Route::post('/store', 'AdminProviderSoundController@store')->name('.store');
        Route::get('/delete/{id}', 'AdminProviderSoundController@destroy')->name('.delete');
    });

    Route::post('/ban/{id}', 'AdminProviderController@ban')->name('.ban');
    Route::post('/unban/{id}', 'AdminProviderController@unban')->name('.unban');

});

Route::prefix('adverts')->namespace('Advert')->name('adverts')->group(function (){
    Route::get('/', 'AdvertController@index')->name('.index');

    Route::post('/approve', 'AdvertController@approve')->name('.approve');
    Route::post('/decline', 'AdvertController@decline')->name('.decline');
});
