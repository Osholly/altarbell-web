<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeneralController;
use Spatie\HttpLogger\Middlewares\HttpLogger;
use App\Http\Controllers\API\v1\Album\ApiAlbumController;
use App\Http\Controllers\API\v1\User\Auth\UserAuthController;
use App\Http\Controllers\API\v1\User\Profile\UserProfileController;
use App\Http\Controllers\API\v1\Notification\NotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware(HttpLogger::class)->group(function () {

    Route::prefix('user')->namespace('User')->group(function () {

        Route::namespace('Auth')->group(function () {

            // Register New User Account
            Route::post('/register', [UserAuthController::class, 'register']);

            // User Login
            Route::post('/login', [UserAuthController::class, 'login']);

            //User 3rd party authentication (Fetching the info from the backend)
            Route::get('/social-login/{provider}', [UserAuthController::class, 'SocialAuthentication'])
                ->where('provider', implode('|', config('auth.socialite.drivers')));

            Route::get('/social-login/{provider}/callback', [UserAuthController::class, 'SocialLogin']);

            // 3rd party authentication (Saving the info from the mobile client)
            Route::post('/social-login', [UserAuthController::class, 'SocialAuth']);

            Route::post('/social-auth-provider', [UserAuthController::class, 'socialAuthProvider']);

            // User Logout
            Route::post('/logout', [UserAuthController::class, 'logout'])->middleware('auth:sanctum');

            ROute::post('/forgot-password', [UserAuthController::class, 'forgot_password']);

            Route::post('/reset-password', [UserAuthController::class, 'reset_password']);
        });

        Route::prefix('profile')->namespace('Profile')->middleware('auth:sanctum')->group(function () {

            Route::post('/update/{id}', [UserProfileController::class, 'update']);
        });
    });

    Route::middleware('auth:sanctum')->group(function () {


        Route::prefix('albums')->namespace('Album')->group(function () {

            Route::get('/', [ApiAlbumController::class, 'index']);

            Route::get('/{id}', [ApiAlbumController::class, 'show']);
        });

        Route::prefix('notifications')->namespace('Notification')->group(function () {

            Route::post('/create', [NotificationController::class, 'store']);

            Route::get('/user/{user_id}', [NotificationController::class, 'user_notifications']);

            Route::get('/{id}', [NotificationController::class, 'show']);

            Route::post('/update/{id}', [NotificationController::class, 'update']);

            Route::post('delete/{id}', [NotificationController::class, 'delete']);
        });
    });

    Route::prefix('adverts')->group(function (){
        Route::post('/view-advert', [GeneralController::class, 'view_advert']);
        Route::post('/unview-advert', [GeneralController::class, 'unview_advert']);
    });

    Route::get('/categories', [GeneralController::class, 'fetch_categories']);

    Route::get('/filter-albums', [GeneralController::class, 'filter_albums']);

    Route::get('/search', [GeneralController::class, 'search']);
});
