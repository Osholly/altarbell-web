<script>
    $('document').ready(function() {

        $('#edit_errors').hide();

        $('#edit-album-btn').click(function(e) {

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            const form = $('#edit-album-form')[0];
            const album_art = $("#album-art")[0].files[0];
            const title = $('#edit_title').val();
            const category_id = $('#edit_category_id').val();
            const album_id = $('#album_id').val();

            console.log(category_id);

            formData = new FormData(form);
            formData.append('title', title);
            formData.append('category_id', category_id);

            if (album_art) {
                formData.append('album_art', album_art);
            }

            console.log(formData);
            console.log(title);
            console.log(category_id);
                // return false;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('provider/albums') }}/"+album_id,
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Updating Album Information',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "Album Update Successful",
                            text: "Album Information Successfully Updated.",
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            window.location = "{{ route('provider.albums.index') }}";
                        }, 3000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                            error = key + ':' + value;
                        });
                        items += '</ul>';
                        $('#edit_0errors').empty();
                        $('#edit_errors').append(items);
                        $('#edit_errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: error,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });

        $('.edit-album').click(function() {

            console.log("test");

            $('#edit_errors').hide();

            const album_id = $(this).attr('data-id');

            $('#album_id').val(album_id);

            const row = $(this).closest(".album");

            const title = row.find('.title').text();

            $('#edit_title').val(title);

            const category_id = row.find('.category').attr('data-category-id');

            $('#edit_category_id').val(category_id);

            const album_art = row.find('.album-art').find('img').attr('src');

            // $('#edit_album_art').attr('data-default-file', "{{ url('') }}"+album_art);
        });

    });

</script>
