@php
$sounds = [];
@endphp

@foreach($album->upcoming_sounds as $sound)
@php
$list = (object) [
'title' => $sound->title,
'start' => date('Y-m-d', strtotime($sound->play_date))
];

array_push($sounds, $list);
@endphp
@endforeach

<script>
    (function($) {
  'use strict';
  $(function() {
    if ($('#calendar').length) {
        var sounds = '<?php echo json_encode($sounds) ?>';
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        defaultDate: '{{ $album->next_sound_play_date() }}',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: JSON.parse(sounds)
      })
    }
  });
})(jQuery);
</script>
