<script>
    $('document').ready(function() {

        $("select").select2({
            placeholder: 'Select an Option',
            width: '100%'
        });

        $('#errors').hide();

        function validate_form(data) {
            var valid_form;
            $.each(data, function(key, field) {
                if (field.value == "") {
                    valid_form = 0;
                    return false;
                } else {
                    valid_form = 1;
                }
            });
            return valid_form;
        }

        $('#create-album-btn').click(function(e) {

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            var form_data = $('#create-album-form').serializeArray();
            var valid_form = validate_form(form_data);

            if (valid_form == 0) {
                swal("Error!", "All Fields are Compulsory!", "error");
                $(this).removeAttr('disabled');
                return false;
            }

            const form = $('#new-album-form')[0];
            const album_art = $("#album-art")[0].files[0];
            const title = $('#title').val();
            const category_id = $('#category_id').val();

            formData = new FormData(form);
            formData.append('title', title);
            formData.append('album_art', album_art);
            formData.append('category_id', category_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('provider.albums.store') }}",
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Creating New Album',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "New Album Created",
                            text: "New Album has been successfully created.",
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            window.location = "{{ route('provider.albums.index') }}";
                        }, 3000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#errors').empty();
                        $('#errors').append(items);
                        $('#errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.message,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });

        $('table').on('click', '.view-album-btn', function() {
            const album_id = $(this).attr('data-id');

            window.location.href = "{{ url('admin/providers/albums') }}/"+album_id;
        });

    });

</script>
