<script>
    $('document').ready(function() {

        $('#errors').hide();

        function validate_form(data) {
            var valid_form;
            $.each(data, function(key, field) {
                if (field.value == "") {
                    valid_form = 0;
                    return false;
                } else {
                    valid_form = 1;
                }
            });
            return valid_form;
        }

        $('#create-category-btn').click(function(e) {

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            var form_data = $('#create-category-form').serializeArray();
            var valid_form = validate_form(form_data);

            if (valid_form == 0) {
                swal("Error!", "All Fields are Compulsory!", "error");
                $(this).removeAttr('disabled');
                return false;
            }

            const form = $('#new-category-form')[0];
            const name = $('#name').val();

            formData = new FormData(form);
            formData.append('name', name);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('admin.categories.store') }}",
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Adding New Album Category',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "New Category Added",
                            text: "New Album Category has been successfully created.",
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            location.reload(true);
                        }, 2000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#errors').empty();
                        $('#errors').append(items);
                        $('#errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.message,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });

    });

</script>
