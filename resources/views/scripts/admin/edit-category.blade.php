<script>
    $('document').ready(function() {

        $('#update-errors').hide();

        function validate_form(data) {
            var valid_form;
            $.each(data, function(key, field) {
                if (field.value == "") {
                    valid_form = 0;
                    return false;
                } else {
                    valid_form = 1;
                }
            });
            return valid_form;
        }

        $('.edit-category-btn').click(function() {


            $('#update-errors').empty();

            const name = $(this).attr('data-name');
            const uuid = $(this).attr('data-id');

            $('#update-name').val(name);
            $('#update-uuid').val(uuid);

        });

        $('#update-category-btn').click(function(e) {

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            var form_data = $('#edit-category-form').serializeArray();
            var valid_form = validate_form(form_data);

            if (valid_form == 0) {
                swal("Error!", "All Fields are Compulsory!", "error");
                $(this).removeAttr('disabled');
                return false;
            }

            const form = $('#update-category-form')[0];
            const name = $('#update-name').val();
            const uuid = $('#update-uuid').val();

            formData = new FormData(form);
            formData.append('name', name);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('admin/categories/update') }}/"+uuid,
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Updating Album Category',
                        allowOutsideClick: false,
                        onOpen: () => {
                            swal.showLoading()
                        },
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "Album Category Updated",
                            text: data.msg,
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            location.reload(true);
                        }, 2000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#update-errors').empty();
                        $('#update-errors').append(items);
                        $('#update-errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.message,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });

    });

</script>
