<script>
    $('document').ready(function() {

        $('#errors').hide();

        function validate_form(data) {
            var valid_form;
            $.each(data, function(key, field) {
                if (field.value == "") {
                    valid_form = 0;
                    return false;
                } else {
                    valid_form = 1;
                }
            });
            return valid_form;
        }

        $('#add-sound-btn').click(function(e) {

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            var form_data = $('#add-sound-form').serializeArray();
            var valid_form = validate_form(form_data);

            if (valid_form == 0) {
                swal("Error!", "All Fields are Compulsory!", "error");
                $(this).removeAttr('disabled');
                return false;
            }

            const form = $('#add-sound-form')[0];
            const file = $("#file")[0].files[0];
            const title = $('#title').val();
            const description = $('#description').val();
            const play_date = $('#play-date').val();
            const album_id = $('#album-id').val();

            formData = new FormData(form);
            formData.append('file', file);
            formData.append('title', title);
            formData.append('description', description);
            formData.append('play_data', play_date);
            formData.append('album_id', album_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('admin.providers.sounds.store') }}",
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {

                    swal({
                        title: 'Adding New Sound to Album',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "New Sound Created",
                            text: "New Sound successfully added to album.",
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            location.reload(true);
                        }, 3000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#errors').empty();
                        $('#errors').append(items);
                        $('#errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.message,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });

        $('.view-sound-btn').click(function() {
            const sound_id = $(this).attr('data-id');

            window.location.href = "{{ url('provider/sounds') }}/"+sound_id;
        });

    });

</script>
