@extends('provider.layouts.layout')

@section('title', 'Running Adverts')

@section('content')
    <!--********************************** Content body start ***********************************-->
    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Running Adverts</h4>
                    </div>
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-sm table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Target</th>
                                        <th>Views</th>
                                        <th>Date added</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($adverts as $advert)
                                        <tr>
                                            <th>{{ $loop->iteration }}</th>
                                            <td>{{ Str::headline($advert->title) }}</td>
                                            <td>{{ $advert->category->name }}</td>
                                            <td>{{ $advert->target }}</td>
                                            <td>{{ $advert->views }}</td>
                                            <td>{{ $advert->created_at->toDayDateTimeString() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--********************************** Content body end ***********************************-->
@endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
    <script>
        $('document').ready(function(){

            $('.decline').click(function () {
                const advert_id = $(this).attr('data-id');

                $('#decline_advert_id').val(advert_id);
            });

            $('.approve').click(function () {
                const advert_id = $(this).attr('data-id');

                $('#approve_advert_id').val(advert_id);

                console.log(advert_id);
                console.log($('#approve_advert_id').val());
            });


            @if(Session::has('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: '{{ session()->get('success') }}'
            })
            @endif

            @if(Session::has('error'))
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: '{{ session()->get('error') }}'
            })
            @endif

        });
    </script>
    <script>
@endpush
