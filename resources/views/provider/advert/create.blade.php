@extends('provider.layouts.layout')
@section('title', 'Create Advert')
@push('styles')
<link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css" rel="stylesheet">
@endpush
@section('content')
    <!--********************************** Content body start ***********************************-->
    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Submit Advert</h4>
                    </div>
                </div>


            </div> <!-- row -->
            <div class="authentication h-100">
                <div class="container-fluid h-100">
                    <div class="row justify-content-center h-100 align-items-center">
                        <div class="col-md-10">
                            <div class="authincation-content">
                                <div class="row no-gutters">
                                    <div class="col-xl-12">
                                        <div class="auth-form">
                                            <h4 class="text-center mb-4">Submit An Advert</h4>
                                            <form method="POST" action="{{ route('provider.adverts.store') }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label><strong>Category</strong></label>
                                                    <select class="form-control" id="edit_category_id" name="category_id" required>
                                                        <option selected disabled>Select an option</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->uuid }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Title</strong></label>
                                                    <input type="text" class="form-control" name="title" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="description"><strong>Content</strong></label>
                                                    <textarea class="textarea_editor form-control bg-transparent" name="content" rows="3" placeholder="Enter Content" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>URL</strong></label>
                                                    <input required type="url" name="url" class="form-control" placeholder="https://www.example.com">
                                                    <small>The url you want viewers to see from the advert.</small>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Image</strong></label>
                                                    <div class="drop-zone">
                                                        <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                                        <input type="file" name="image" class="drop-zone__input" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Start Date</strong></label>
                                                    <input type="date" min="{{ now()->addDay()->format('Y-m-d') }}" class="form-control" required name="start_date">
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Duration</strong></label>
                                                    <input type="number" min="1" name="duration" required class="form-control">
                                                    <small>Number of days for the advert campaign to run.</small>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Targeted Views</strong></label>
                                                    <input type="number" min="1" class="form-control" required name="target">
                                                    <small>Number of targeted views</small>
                                                </div>
                                                <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                                </div>
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--********************************** Content body end ***********************************-->
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
    <script>
        $('document').ready(function(){

            @if(Session::has('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: '{{ session()->get('success') }}'
            })

            @if(Session::has('link'))
            setTimeout(function(){
                window.location = "{{ session()->get('link') }}";
            }, 3000);
            @endif

            @endif

            @if(Session::has('error'))
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: '{{ session()->get('error') }}'
            })
            @endif

        });
    </script>
@endpush
