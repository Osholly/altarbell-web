<!--**********************************
        Nav header start
    ***********************************-->
<div class="nav-header">
    <a href="" class="brand-logo">
        <img class="logo-abbr" src="{{ asset('images/logo.png') }}" alt="">
        <img class="logo-compact" src="{{ asset('images/logotext.png') }}" alt="">
        <img class="brand-title" src="{{ asset('images/logotext.png') }}" alt="">
    </a>

    <div class="nav-control">
        <div class="hamburger">
            <span class="line"></span><span class="line"></span><span class="line"></span>
        </div>
    </div>
</div>
<!--**********************************
    Nav header end
***********************************-->

<!--**********************************
    Header start
***********************************-->
<div class="header">
    <div class="header-content">
        <nav class="navbar navbar-expand">
            <div class="collapse navbar-collapse justify-content-between">
                <div class="header-left">
                </div>

                <ul class="navbar-nav header-right">
                    <li class="nav-item dropdown notification_dropdown">
                        <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                            <i class="mdi mdi-bell"></i>
                            <div class="pulse-css"></div>
                        </a>
                    </li>
                    <li class="nav-item dropdown header-profile">
                        <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                            <img src="{{ asset('images/avatar/1.png') }}" alt="">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('provider.logout') }}" onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();" class="dropdown-item">
                                <i class="icon-key"></i>
                                <span class="ml-2">Logout </span>
                            </a>
                        </div>
                        <form id="logout-form" action="{{ route('provider.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!--**********************************
    Header end ti-comment-alt
***********************************-->
