<!--**********************************
        Footer start
    ***********************************-->
<div class="footer">
    <div class="copyright">
        <p>Copyright © {{ date('Y') }} <a href="https://nqb8.co/" target="_blank">NQB8 Group.</a> All Rights Reserved</p>
    </div>
</div>
<!--**********************************
    Footer end
***********************************-->
