<!--**********************************
        Sidebar start
    ***********************************-->
<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            <li>
                <a class="" href="{{ route('provider.home') }}" aria-expanded="false"><i
                        class="icon icon-app-store"></i><span class="nav-text">Dashboard</span></a>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon icon-single-04"></i><span
                        class="nav-text">My Profile</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('provider.profile.show', ['id' => Auth::guard('provider')->user()->uuid]) }}">View Profile</a></li>
                    <li><a href="{{ route('provider.profile.edit', ['id' => Auth::guard('provider')->user()->uuid]) }}">Edit Profile</a></li>
                </ul>
            </li>
            <li>
                <a class="" href="{{ route('provider.albums.index') }}" aria-expanded="false"><i class="fas fa-book-open"></i><span
                        class="nav-text">My Albums</span></a>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon icon-single-04"></i><span
                        class="nav-text">My Adverts</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('provider.adverts.create') }}">Create Adverts</a></li>
                    <li><a href="{{ route('provider.adverts.running') }}">Running Adverts</a></li>
                    <li><a href="{{ route('provider.adverts.index') }}">Advert History</a></li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon icon-single-04"></i><span
                        class="nav-text">Transactions</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('provider.payment-methods.index') }}">Payment Methods</a></li>
                    <li><a href="{{ route('provider.transactions.index') }}">Transaction History</a></li>
                </ul>
            </li>
        <ul>


    </div>


</div>
<!--**********************************
    Sidebar end
***********************************-->
