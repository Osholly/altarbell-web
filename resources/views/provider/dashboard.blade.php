@extends('provider.layouts.layout')

@section('title', 'Provider Dashboard')

@section('content')
{{--<div class="content-wrapper">--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12 grid-margin">--}}
{{--            <div class="row">--}}
{{--                <div class="col-12 col-xl-5 mb-4 mb-xl-0">--}}
{{--                    <h4 class="font-weight-bold">Hello,--}}
{{--                        {{ Auth::guard('provider')->user()->title." ".Auth::guard('provider')->user()->first_name }}!--}}
{{--                    </h4>--}}
{{--                    <h4 class="font-weight-normal mb-0">Your Dashboard</h4>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Number of Albums</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ Auth::guard('provider')->user()->albums->count() }}</h3>--}}
{{--                        <i class="ti-book icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Number of Sounds</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ Auth::guard('provider')->user()->sounds->count() }}</h3>--}}
{{--                        <i class="ti-music-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Total Number of Active Notifications</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ Auth::guard('provider')->user()->notifications->where('active', true)->count() }}</h3>--}}
{{--                        <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- content-wrapper ends -->
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Hello, {{ Auth::guard('provider')->user()->title." ".Auth::guard('provider')->user()->first_name }}</h4>
                    <p class="mb-0">Your dashboard</p>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <a href="albums.html">
                            <div class="stat-content">
                                <div class="stat-text">Number of Albums </div>
                                <div class="stat-digit">{{ Auth::guard('provider')->user()->albums->count() }}</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Number of Songs </div>
                            <div class="stat-digit">{{ Auth::guard('provider')->user()->sounds->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Number of Active Subscriptions </div>
                            <div class="stat-digit">{{ Auth::guard('provider')->user()->notifications->where('active', true)->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
@endsection

