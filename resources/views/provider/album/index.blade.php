@extends('provider.layouts.layout')

@section('title', 'My Albums')

@section('content')

<!--********************************** Content body start ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>My Album</h4>
                </div>
            </div>

            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <div class="bootstrap-modal">
                    <!-- Button trigger modal --> <button type="button" class="btn btn-primary" data-toggle="modal"
                                                          data-target="#basicModal">Add New Album</button> <!-- Modal -->
                    <div class="modal fade" id="basicModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Create New Album</h5> <button type="button" class="close"
                                                                                          data-dismiss="modal"><span>&times;</span> </button>
                                </div>
                                <div class="modal-body">
                                    <form id="create-album-form">
                                        <div id="errors" class="alert alert-danger"></div>
                                        <div class="form-group">
                                            <label><strong>Title</strong></label>
                                            <input type="text" class="form-control" id="title" required name="title" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" id="category_id" name="category_id">
                                                <option selected disabled>Select an Option</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->uuid }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label>Art Album</label>
                                        <div class="drop-zone">
                                            <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                            <input type="file" id="album-art" name="album_art" class="drop-zone__input">
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="create-album-btn">Create Album</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="editModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Album</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span> </button>
                                </div>
                                <div class="modal-body">
                                    <form id="edit-album-form">
                                        <div id="edit_errors" class="alert alert-danger"></div>
                                        <input type="hidden" id="album_id" name="album_id">
                                        <div class="form-group">
                                            <label><strong>Title</strong></label>
                                            <input type="text" id="edit_title" name="title" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" id="edit_category_id" name="category_id">
                                                <option selected disabled>Select Category</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->uuid }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                            <small>Leave blank to maintain current category.</small>
                                        </div>
                                        <div class="mb-3"> <label for="formFile" class="form-label">Album Art</label>
                                            <div class="drop-zone">
                                                <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                                <input type="file" id="edit_album_art" name="album_art" class="drop-zone__input">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="edit-album-btn">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- row -->
        <div class="row">
            @if($albums->isEmpty())
                <p class="text-center">No albums created yet. Kindly create a new album to get started.</p>
            @else
                @foreach($albums as $album)
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ Storage::url($album->album_art) }}" class="card-img-top album-art" alt="...">
                            <div class="card-body album">
                                <h5 class="card-title title">{{ Str::title($album->title) }}</h5>
                                <p class="card-text category" data-category-id="{{ $album->category_id }}">{{ Str::ucfirst($album->category->name) }}</p>
                                <button class="badge badge-primary view-album-btn"
                                   data-id="{{ $album->uuid }}">View Sounds</button>
                                <button href="javascript:void()" class="badge badge-warning text-white edit-album" data-id="{{ $album->uuid }}"
                                        id="edit_album" data-toggle="modal"
                                        data-target="#editModal">Edit</button>
                            </div>
                        </div>
                    </div>
                @endforeach
                @endif
        </div>
    </div>
    {{ $albums->links() }}
</div>
<!--********************************** Content body end ***********************************-->

{{-- Create album modal --}}
{{--<div class="modal fade" id="create-album-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"--}}
{{--    aria-hidden="true">--}}
{{--    <div class="modal-dialog" role="document">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <h5 class="modal-title" id="ModalLabel">Create New Album</h5>--}}
{{--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                    <span aria-hidden="true">&times;</span>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body">--}}
{{--                <form id="create-album-form">--}}
{{--                    <div id="errors" class="alert alert-danger"></div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="title" class="col-form-label">Title</label>--}}
{{--                        <input type="text" class="form-control" id="title" required name="title">--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label>Category</label>--}}
{{--                        <select class="form-control" id="category_id" name="category_id">--}}
{{--                            <option></option>--}}
{{--                            @foreach($categories as $category)--}}
{{--                            <option value="{{ $category->uuid }}">{{ $category->name }}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label>Album Art</label>--}}
{{--                        <div class="drop-zone">--}}
{{--                            <span class="drop-zone__prompt">Drop file here or click to upload</span>--}}
{{--                            <input type="file" id="album-art" name="album_art" class="drop-zone__input">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--            <div class="modal-footer">--}}
{{--                <button type="button" class="btn btn-success" id="create-album-btn">Create Album</button>--}}
{{--                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

@push('scripts')

<!-- Plugin js for this page-->
<script src="{{ asset('vendors/sweetalert/sweetalert.min.js') }}"></script>
<!-- End plugin js for this page-->

<!-- Custom js for this page-->
@include('scripts.provider.create-album');
@include('scripts.provider.edit-album');
<!-- End custom js for this page-->
@endpush
@endsection
