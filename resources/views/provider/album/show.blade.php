@extends('provider.layouts.layout')

@section('title', $album->title.' Preview')

@section('content')

@push('styles')

<!-- Plugin css for this page -->
{{--<link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">--}}
{{--<link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">--}}
{{--<link rel="stylesheet" href="{{ asset('vendors/dropify/dropify.min.css') }}">--}}
{{--<link rel="stylesheet" href="{{ asset('css/amplitudejs/amplitudejs.css') }}">--}}
{{--<link rel="stylesheet" href="{{ asset('vendors/fullcalendar/fullcalendar.min.css') }}">--}}
@endpush
<!--********************************** Content body start ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>{{ $album->title }}</h4>
                </div>
            </div>

            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <div class="bootstrap-modal">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicModal">Add New Sounds</button>
                    <!-- Modal -->
                    <div class="modal fade" id="basicModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add New Sound</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span> </button>
                                </div>
                                <div class="modal-body">
                                    <form id="add-sound-form">
                                        <div id="errors" class="alert alert-danger"></div>
                                        <div class="form-group">
                                            <label><strong>Album</strong></label>
                                            <input type="text" id="album" value="{{ $album->title }}" class="form-control" required>
                                            <input type="hidden" value="{{ $album->uuid }}" class="form-control" id="album-id" required
                                                   name="album_id">
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Sound Title</strong></label>
                                            <input type="text" id="title" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea class="textarea_editor form-control bg-transparent" rows="3"
                                                      placeholder="Enter Description" id="description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Play date</strong></label>
                                            <input type="date" id="play_date" value="{{ $album->upcoming_sounds->count() > 0 ? $album->next_available_sound_date() : ''}}"
                                                   name="play_date" min="{{ date('Y-m-d') }}"
                                                   {{ $album->upcoming_sounds->count() > 0 ? 'readonly' : ''}} class="form-control" required>
                                        </div>
                                        <label>Album Art</label>
                                        <div class="drop-zone">
                                            <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                            <input type="file" id="file" name="file" required class="drop-zone__input">
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="add-sound-btn">Add Sound</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="editModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Album</h5> <button type="button" class="close"
                                                                                    data-dismiss="modal"><span>&times;</span> </button>
                                </div>
                                <div class="modal-body">
                                    <form action="#">

                                        <div class="form-group"> <label><strong>Title</strong></label> <input type="text"
                                                                                                              value="Heavy is the head" class="form-control" required> </div>
                                        <div class="form-group"> <label>Category</label> <select class="form-control"
                                                                                                 id="edit_category_id" name="category_id">
                                                <option selected disabled>Select an option</option>
                                                <option value="f07bd2e0-8d61-11eb-8cc2-ddcda0409cde">DayStar</option>
                                                <option value="bba59210-8d65-11eb-9961-f1adc705818a">Winners</option>
                                                <option value="a1b8a600-8d6a-11eb-aab9-01f5fe73ba89">Inspiration</option>
                                                <option value="aebc3b00-8d6a-11eb-b813-d3663504d461">Motivation</option>
                                                <option value="7679aee0-8e19-11eb-94dd-b740c2f2042c">Devotional</option>
                                                <option value="852e2ba0-8e19-11eb-9901-b7e3c91fb310">Leadership</option>
                                                <option value="95122c60-8e19-11eb-912c-67d36d532929">Business &amp; Career</option>
                                                <option value="a2843f60-8e19-11eb-8dec-f302d9d336be">Faith</option>
                                                <option value="ad4f2740-8e19-11eb-b3a8-71af97f275b0">Finance</option>
                                                <option value="8121b180-8e1a-11eb-980d-07c6f0942ed1">Health</option>
                                                <option value="90401520-8e1a-11eb-abb2-1508ecb90b10">Praise &amp; Worship</option>
                                                <option value="4775a960-8e22-11eb-a48e-4b0a3faeffcf">Redeemed</option>
                                                <option value="0e62f990-a384-11eb-afdf-81f12e60b259">Sermon</option>
                                            </select>
                                            <small>leave blank to maintain the current category</small>
                                        </div>

                                        <div class="mb-3"> <label for="formFile" class="form-label">Album Art</label> <input
                                                class="form-control" type="file" id="formFile"> </div>
                                    </form>
                                </div>
                                <div class="modal-footer"> <button type="button" class="btn btn-secondary"
                                                                   data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="row my-4 mx-3">
                        <div class="col-md-3">
                            <img
                                src="{{ Storage::url($album->album_art) }}"
                                class="card-img-top album-art album-art-main" alt="...">
                        </div>
                        <div class="col-md-6 mt-5">
                            <p class="font-weight-bold"><PLAYLIST></PLAYLIST></p>
                            <h2 class="text-primary">{{ $album->title }}</h2>
                            <p>{{ $album->upcoming_sounds->count()." upcoming sounds"}} </p>
                        </div>
                    </div>
                    <div class="card-body">

                        @if($album->upcoming_sounds->count() > 0)
                        <div class="table-responsive">
                                <table class="table table-responsive-sm table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Album</th>
                                        <th>Date added</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($album->upcoming_sounds as $sound)
                                        <tr>
                                            <th>{{ $loop->iteration }}</th>
                                            <td> <img src="{{ Storage::url($album->album_art) }}" width="30" class="img-fluid mr-3" alt=""> {{ $sound->title }}</td>
                                            <td>{{ $sound->description }}</td>
                                            <td>{{ $sound->created_at->toDayDateTimeString() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--********************************** Content body end ***********************************-->

{{--<div class="content-wrapper">--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12 grid-margin">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <h3>{{ $album->title }}</h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--    <div class="row grid-margin">--}}
{{--        <div class="col-md-12 grid-margin">--}}
{{--            <div class="card">--}}
{{--                <div class="col-lg-12">--}}
{{--                    <h4 class="mt-5">Upcoming Sounds</h4>--}}
{{--                    <div class="mb-3 float-right">--}}
{{--                        <button class="btn btn-success rounded-0" id="add-sound" data-toggle="modal"--}}
{{--                            data-target="#create-sound-modal">Add New Sound</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-12">--}}
{{--                            @if($album->upcoming_sounds->count() > 0)--}}
{{--                            <div class="table-responsive">--}}
{{--                                <table id="order-listing" class="table">--}}
{{--                                    <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th>SN</th>--}}
{{--                                            <th>Title</th>--}}
{{--                                            <th>Description</th>--}}
{{--                                            <th>Play Date</th>--}}
{{--                                            <th>Date Created</th>--}}
{{--                                            <th>Last Updated</th>--}}
{{--                                            <th>Actions</th>--}}
{{--                                        </tr>--}}
{{--                                    </thead>--}}
{{--                                    <tbody>--}}
{{--                                        @foreach($album->upcoming_sounds as $sound)--}}
{{--                                        <tr>--}}
{{--                                            <td>{{ $loop->iteration }}</td>--}}
{{--                                            <td class="title">{{ $sound->title }}</td>--}}
{{--                                            <td class="description">{{ $sound->description }}</td>--}}
{{--                                            <td class="play_date">{{ $sound->play_date->format('l jS \of F Y') }}</td>--}}
{{--                                            <td>{{ $sound->created_at->toDayDateTimeString() }}</td>--}}
{{--                                            <td>{{ $sound->updated_at->diffForHumans() }}</td>--}}
{{--                                            <td>--}}
{{--                                                <button class="btn btn-outline-primary pull-left view-sound-btn"--}}
{{--                                                    data-id="{{ $sound->uuid }}">View</button>--}}
{{--                                                <button class="btn btn-outline-warning edit-sound"--}}
{{--                                                    data-id="{{ $sound->uuid }}" data-toggle="modal"--}}
{{--                                                    data-target="#edit-sound-modal">Edit</button>--}}
{{--                                                --}}{{-- <button class="btn btn-outline-danger pull-right">Delete</button> --}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                        @endforeach--}}
{{--                                    </tbody>--}}
{{--                                </table>--}}
{{--                            </div>--}}
{{--                            @else--}}
{{--                            <p>No soundtrack uploaded for this album yet.</p>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    // Music Player--}}
{{--    @if($album->upcoming_sounds->count() > 0)--}}
{{--    <div class=" row">--}}
{{--        <div class="col-md-12 grid-margin">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <h4 class="mt-5">Preview Upcoming Sounds</h4>--}}
{{--                            <!-- Blue Playlist Container -->--}}
{{--                            <div id="blue-playlist-container">--}}

{{--                                <!-- Amplitude Player -->--}}
{{--                                <div id="amplitude-player">--}}

{{--                                    <!-- Left Side Player -->--}}
{{--                                    <div id="amplitude-left">--}}
{{--                                        <img data-amplitude-song-info="cover_art_url" class="album-art" />--}}
{{--                                        <div class="amplitude-visualization" id="large-visualization">--}}

{{--                                        </div>--}}
{{--                                        <div id="player-left-bottom">--}}
{{--                                            <div id="time-container">--}}
{{--                                                <span class="current-time">--}}
{{--                                                    <span class="amplitude-current-minutes"></span>:<span--}}
{{--                                                        class="amplitude-current-seconds"></span>--}}
{{--                                                </span>--}}
{{--                                                <div id="progress-container">--}}
{{--                                                    <div class="amplitude-wave-form">--}}

{{--                                                    </div>--}}
{{--                                                    <input type="range" class="amplitude-song-slider" />--}}
{{--                                                    <progress id="song-played-progress"--}}
{{--                                                        class="amplitude-song-played-progress"></progress>--}}
{{--                                                    <progress id="song-buffered-progress"--}}
{{--                                                        class="amplitude-buffered-progress" value="0"></progress>--}}
{{--                                                </div>--}}
{{--                                                <span class="duration">--}}
{{--                                                    <span class="amplitude-duration-minutes"></span>:<span--}}
{{--                                                        class="amplitude-duration-seconds"></span>--}}
{{--                                                </span>--}}
{{--                                            </div>--}}

{{--                                            <div id="control-container">--}}
{{--                                                <div id="repeat-container">--}}
{{--                                                    <div class="amplitude-repeat" id="repeat"></div>--}}
{{--                                                    <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle">--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                                <div id="central-control-container">--}}
{{--                                                    <div id="central-controls">--}}
{{--                                                        <div class="amplitude-prev" id="previous">--}}
{{--                                                        </div>--}}
{{--                                                        <div class="amplitude-play-pause" id="play-pause">--}}
{{--                                                        </div>--}}
{{--                                                        <div class="amplitude-next" id="next"></div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                                <div id="volume-container">--}}
{{--                                                    <div class="volume-controls">--}}
{{--                                                        <div class="amplitude-mute amplitude-not-muted">--}}
{{--                                                        </div>--}}
{{--                                                        <input type="range" class="amplitude-volume-slider" />--}}
{{--                                                        <div class="ms-range-fix">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="amplitude-shuffle amplitude-shuffle-off"--}}
{{--                                                        id="shuffle-right">--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            <div id="meta-container">--}}
{{--                                                <span data-amplitude-song-info="name" class="song-name"></span>--}}
{{--                                                <span amplitude-song-info="genre"></span>--}}
{{--                                                <div class="song-artist-album">--}}
{{--                                                    <span data-amplitude-song-info="artist"></span>--}}
{{--                                                    <span data-amplitude-song-info="album"></span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- End Left Side Player -->--}}

{{--                                    <!-- Right Side Player -->--}}
{{--                                    <div id="amplitude-right">--}}
{{--                                        @foreach($album->upcoming_sounds as--}}
{{--                                        $sound)--}}
{{--                                        <div class="song amplitude-song-container amplitude-play-pause"--}}
{{--                                            data-amplitude-song-index="{{ $loop->index }}">--}}
{{--                                            <div class="song-now-playing-icon-container">--}}
{{--                                                <div class="play-button-container">--}}
{{--                                                </div>--}}
{{--                                                <img class="now-playing"--}}
{{--                                                    src="https://521dimensions.com/img/open-source/amplitudejs/blue-player/now-playing.svg" />--}}
{{--                                            </div>--}}
{{--                                            <div class="song-meta-data">--}}
{{--                                                <span class="song-title">{{ $sound->title }}</span>--}}
{{--                                                <span--}}
{{--                                                    class="song-artist">{{ $sound->album->provider->first_name.' '.$sound->album->provider->last_name }}</span>--}}
{{--                                            </div>--}}
{{--                                            <audio class="manual-audio" id="{{ $sound->uuid }}"--}}
{{--                                                src="{{ Storage::url($sound->path) }}"></audio>--}}
{{--                                            <span class="song-duration">{{ $sound->play_date->format('d/m') }}</span>--}}
{{--                                        </div>--}}
{{--                                        @endforeach--}}
{{--                                    </div>--}}
{{--                                    <!-- End Right Side Player -->--}}
{{--                                </div>--}}
{{--                                <!-- End Amplitdue Player -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @include('provider.album.calendar')--}}
{{--    @endif--}}

{{--    --}}{{-- Create sound modal --}}
{{--    <div class="modal fade" id="create-sound-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"--}}
{{--        aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="ModalLabel">Add New Sound</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    <form id="add-sound-form">--}}
{{--                        <div id="errors" class="alert alert-danger"></div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="album" class="col-form-label">Album</label>--}}
{{--                            <input type="text" value="{{ $album->title }}" readonly class="form-control" id="album"--}}
{{--                                required name="album_id">--}}
{{--                            <input type="hidden" value="{{ $album->uuid }}" class="form-control" id="album-id" required--}}
{{--                                name="album_id">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="title" class="col-form-label">Sound Title</label>--}}
{{--                            <input type="text" class="form-control" id="title" required name="title">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="title" class="col-form-label">Sound Description</label>--}}
{{--                            <textarea name="description" id="description" class="form-control" rows="5"--}}
{{--                                placeholder="description"></textarea>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="play-date" class="col-form-label">Play Date</label>--}}
{{--                            <div id="datepicker-popup" class="input-group date datepicker">--}}
{{--                                <input type="date" id="play-date"--}}
{{--                                    value="{{ $album->upcoming_sounds->count() > 0 ? $album->next_available_sound_date() : ''}}"--}}
{{--                                    name="play_date" min="{{ date('Y-m-d') }}" class="form-control" required--}}
{{--                                    {{ $album->upcoming_sounds->count() > 0 ? 'readonly' : ''}}>--}}
{{--                                <span class=" input-group-addon input-group-append border-left">--}}
{{--                                    <span class="ti-calendar input-group-text"></span>--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>Audio File</label>--}}
{{--                            <input type="file" class="dropify" data-height="250" id="file" name="file" required />--}}
{{--                            <small>Kindly note that supported file formats are MP3, M4A, FLAC,--}}
{{--                                WAV and AAC</small>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-success" id="add-sound-btn">Add New--}}
{{--                        Sound</button>--}}
{{--                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{-- Edit sound modal --}}
{{--    <div class="modal fade" id="edit-sound-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"--}}
{{--        aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="ModalLabel">Edit Sound Info</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    <form id="edit-sound-form">--}}
{{--                        <div id="edit_errors" class="alert alert-danger"></div>--}}
{{--                        <div class="form-group">--}}
{{--                            --}}{{-- <label for="album" class="col-form-label">Album</label> --}}
{{--                            <input type="hidden" value="{{ $album->title }}" readonly class="form-control"--}}
{{--                                id="edit_album" required name="album_id">--}}
{{--                            <input type="hidden" value="{{ $album->uuid }}" class="form-control" id="edit-album-id"--}}
{{--                                required name="album_id">--}}
{{--                            <input type="hidden" class="form-control" id="sound_id" required>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="title" class="col-form-label">Sound Title</label>--}}
{{--                            <input type="text" class="form-control" id="edit_title" required name="title">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="title" class="col-form-label">Sound Description</label>--}}
{{--                            <textarea name="description" id="edit_description" class="form-control" rows="5"--}}
{{--                                placeholder="description"></textarea>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="play-date" class="col-form-label">Play Date</label>--}}
{{--                            <div class="input-group date datepicker">--}}
{{--                                <input type="date" id="play_date" min="{{ date('Y-m-d') }}" class="form-control"--}}
{{--                                    required>--}}
{{--                                <span class=" input-group-addon input-group-append border-left">--}}
{{--                                    <span class="ti-calendar input-group-text"></span>--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>Audio File</label>--}}
{{--                            <input type="file" class="dropify" data-height="250" id="file" name="file" />--}}
{{--                            <small>Leave empty to maintain the original soundtrack</small>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-warning" id="edit-sound-btn">Update--}}
{{--                        Sound</button>--}}
{{--                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


@push('scripts')

<!-- Plugin js for this page-->
<script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('vendors/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('vendors/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('vendors/amplitudejs/amplitude.min.js') }}"></script>
<script src="{{ asset('vendors/moment/moment.min.js') }}"></script>
<script src="{{ asset('vendors/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<!-- End plugin js for this page-->

<!-- Custom js for this page-->
<script src="{{ asset('js/formpickers.js') }}"></script>
<script src="{{ asset('js/data-table.js') }}"></script>
<script src="{{ asset('js/dropify.js') }}"></script>
@if($album->upcoming_sounds->count() > 0)
@include('scripts.provider.calendar', ['album' => $album])
@include('scripts.provider.amplitudejs', ['sounds' => $album->upcoming_sounds])
@endif
<script>
    $('#errors').hide();

    $('table').on('click', '.edit-sound', function() {

        $('#edit_errors').hide();

        var sound_id = $(this).attr('data-id');

        const row = $(this).closest('tr');

        const title = row.find('.title').text();

        const description = row.find('.description').text();

        const play_date = row.find('.play_date').text();

        $('#sound_id').val(sound_id);

        $('#edit_title').val(title);

        $('#edit_description').val(description);

        $('#play_date').val(play_date);

    });

    $('#edit-sound-btn').click(function(e) {

        e.preventDefault();

        $(this).attr('disabled', 'disabled');

        const form = $('#edit-sound-form')[0];
        const file = $("#file")[0].files[0];
        const title = $('#edit_title').val();
        const sound_id = $('#sound_id').val();
        const description = $('#edit_description').val();
        const album_id = "{{ $album->uuid }}";
        const play_date = $('#play_date').val();

        formData = new FormData(form);
        formData.append('title', title);
        formData.append('description', description);
        formData.append('album_id', album_id);
        formData.append('play_date', play_date);

        if (file) {
        formData.append('file', file);
        }

        console.log(formData);

        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('provider/sounds') }}/"+sound_id,
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Updating Sound Information',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "Sound Update Successful",
                            text: "Sound Information Successfully Updated.",
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            location.reload(true);
                        }, 3000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#edit_errors').empty();
                        $('#edit_errors').append(items);
                        $('#edit_errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.message,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });

    });

</script>
@include('scripts.provider.add-sound')
<!-- End custom js for this page-->
@endpush
@endsection
