@extends('provider.layouts.layout')
@section('title', 'Payment Methods')
@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css" rel="stylesheet">
@endpush
@section('content')
    <!--********************************** Content body start ***********************************-->
    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Payment Method</h4>
                    </div>
                </div>


            </div> <!-- row -->
            <div class="authentication h-100">
                <div class="container-fluid h-100">
                    <div class="row justify-content-center h-100 align-items-center">
                        <div class="col-md-10">
                            <div class="authincation-content">
                                <div class="row no-gutters">
                                    <div class="col-xl-12">
                                        <div class="auth-form">
                                            <h4 class="text-center mb-4">Saved Payment Method</h4>
                                            <form method="POST" action="{{ route('provider.payment-methods.store') }}">
                                                    @csrf
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-success btn-block">Add Payment Method</button>
                                                        <small><b>Kindly note that you will be debited a sum of NGN50 for this operation.</b></small>
                                                    </div>
                                                </form>
                                        </div>
                                        @if($payment_method != null)
                                            <div class="col-md-12 text-center">
                                                <h6>Card number:</h6>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <img src="{{ $payment_method->brand == "mastercard" ? "https://upload.wikimedia.org/wikipedia/commons/a/a4/Mastercard_2019_logo.svg" : "https://cdn.visa.com/v2/assets/images/logos/visa/blue/logo.png" }}" width="50" alt="">
                                                        <span class="mx-3">{{ Str::padLeft($payment_method->last4, 16, '*' ) }}</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>Expiry Date:</h6>
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <span>{{ $payment_method->exp_month."/".substr($payment_method->exp_year, -2) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6>Bank</h6>
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <span>{{ $payment_method->bank }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--********************************** Content body end ***********************************-->
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
    <script>
        $('document').ready(function(){

            @if(Session::has('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: '{{ session()->get('success') }}'
            })
            @endif

            @if(Session::has('link'))
            setTimeout(function(){
                window.location = "{{ session()->get('link') }}";
            }, 3000);
            @endif

            @if(Session::has('error'))
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: '{{ session()->get('error') }}'
            })
            @endif

            @if(Session::has('info'))
            Swal.fire({
                icon: 'info',
                title: 'Notice',
                text: '{{ session()->get('info') }}'
            })
            @endif

        });
    </script>
@endpush
