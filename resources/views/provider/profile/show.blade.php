@extends('provider.layouts.layout')

@section('title', 'My Profile')

@section('content')
<!--**********************************
        Content body start
    ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Hi, {{ $provider->first_name }}</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Profile</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="profile">
                    <div class="profile-head">
                        <div class="photo-content">
                            <div class="cover-photo"></div>
                            <div class="profile-photo">
                                <img src="{{ Storage::url($provider->avatar) }}" class="img-fluid rounded-circle" alt="">
                            </div>
                        </div>
                        <div class="profile-info">
                            <div class="row justify-content-center">
                                <div class="col-xl-8">
                                    <div class="row">
                                        <div class="col-xl-4 col-sm-4 border-right-1 prf-col">
                                            <div class="profile-name">
                                                <h4 class="text-primary p-2">{{ $provider->title.' '.$provider->first_name.' '.$provider->last_name }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-personal-info">
                            <h4 class="text-primary mb-4">Personal Information</h4>
                            <div class="row mb-4">
                                <div class="col-5">
                                    <h6 class="f-w-500">Name</h6>
                                </div>
                                <div class="col-7"><span>{{ "$provider->first_name $provider->last_name " }}</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-5">
                                    <h6 class="f-w-500">Email
                                    </h6>
                                </div>
                                <div class="col-7"><span>{{ $provider->email }}</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-5">
                                    <h6 class="f-w-500">Phone
                                    </h6>
                                </div>
                                <div class="col-7"><span>{{ $provider->phone }}</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-5">
                                    <h6 class="f-w-500">Location</h6>
                                </div>
                                <div class="col-7"><span>{{ $provider->address }}</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-5">
                                    <h6 class="f-w-500">Organization</h6>
                                </div>
                                <div class="col-7"><span>{{ $provider->organization }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
@endsection
