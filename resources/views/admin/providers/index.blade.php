@extends('admin.layouts.layout')

@section('title', 'Content Providers')

@section('content')
@push('styles')

<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">

<!-- End plugin css for this page -->
@endpush

<div class="content-wrapper">
    <div class="card">
        <div class="col-lg-12">
            <h4 class="card-title mt-5">Content Providers</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    @if($providers->isEmpty())
                    <p class="text-center">No content provider found.
                    </p>
                    @else
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Album Count</th>
                                    <th>Member Since</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($providers as $provider)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><img src="{{ Storage::url($provider->avatar) }}"></td>
                                    <td class="name">{{ $provider->first_name.' '.$provider->last_name }}</td>
                                    <td>{{ $provider->albums->count() }}</td>
                                    <td>{{ $provider->created_at->diffForHumans() }}</td>
                                    <td>
                                        @if($provider->isBanned())
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-outline-primary"
                                            href="{{ route('admin.providers.show', ['id' => $provider->uuid]) }}">View</a>
                                        @if($provider->isBanned())
                                        <button class="btn btn-outline-success unban-provider"
                                            data-id="{{ $provider->uuid }}" data-toggle="modal"
                                            data-target="#unban-provider-modal">Unban</button>
                                        @else
                                        <button class="btn btn-outline-danger ban-provider"
                                            data-id="{{ $provider->uuid }}" data-toggle="modal"
                                            data-target="#ban-provider-modal">Ban</button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->


<div class="modal fade" id="ban-provider-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Ban Provider</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="ban-provider-form">
                    <div id="errors" class="alert alert-danger"></div>
                    <div class="form-group">
                        <label for="name" class="col-form-label">Provider Name</label>
                        <input type="text" class="form-control" id="name" readonly>
                        <input type="hidden" id="provider_id">
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-form-label">Ban Expiration Date</label>
                        <input type="date" class="form-control" id="expired_at">
                        <input type="hidden" id="provider_id">
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-form-label">Comment</label>
                        <textarea rows="5" class="form-control" id="comment"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="ban-provider-btn">Ban Provider</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="unban-provider-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">UnBan Provider</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="unban-provider-form">
                    <div id="unban_errors" class="alert alert-danger"></div>
                    <div class="form-group">
                        <label for="name" class="col-form-label">Provider Name</label>
                        <input type="text" class="form-control" id="unban_name" readonly>
                        <input type="hidden" id="unban_provider_id">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="unban-provider-btn">UnBan Provider</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@push('scripts')

<!-- Plugin js for this page-->
<script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('vendors/sweetalert/sweetalert.min.js') }}"></script>
<!-- End plugin js for this page-->

<!-- Custom js for this page-->
<script src="{{ asset('js/data-table.js') }}"></script>
@include('scripts.admin.create-category')
@include('scripts.admin.edit-category')

<script>
    $('document').ready(function() {
        $('table').on('click', '.ban-provider', function(){

            $('#errors').hide();

            const provider_id = $(this).attr('data-id');

            const row = $(this).closest('tr');

            const name = row.find('.name').text();

            $('#provider_id').val(provider_id);

            $('#name').val(name);

        });

        $('#ban-provider-btn').click(function(e){

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            const form = $('#ban-provider-form')[0];
            const provider_id = $('#provider_id').val();
            const comment = $('#comment').val();
            const expired_at = $('#expired_at').val();

            formData = new FormData(form);
            formData.append('provider_id', provider_id);
            formData.append('comment', comment);

            console.log(provider_id);
            console.log(comment);
            console.log(provider_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('admin/providers/ban') }}/"+provider_id,
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Banning Provider Account',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "Ban Successful",
                            text: "Provider account successfully banned.",
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            location.reload(true);
                        }, 3000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#errors').empty();
                        $('#errors').append(items);
                        $('#errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.msg,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });

        $('table').on('click', '.unban-provider', function(){

            $('#unban_errors').hide();

            const provider_id = $(this).attr('data-id');

            const row = $(this).closest('tr');

            const name = row.find('.name').text();

            $('#unban_provider_id').val(provider_id);

            $('#unban_name').val(name);

        });

        $('#unban-provider-btn').click(function(e){

            e.preventDefault();

            $(this).attr('disabled', 'disabled');

            const form = $('#unban-provider-form')[0];
            const provider_id = $('#unban_provider_id').val();

            formData = new FormData(form);
            formData.append('provider_id', provider_id);
            formData.append('comment', comment);

            console.log(provider_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('admin/providers/unban') }}/"+provider_id,
                processData: false,
                contentType: false,
                method: "POST",
                data: formData,
                beforeSend: function() {
                    swal({
                        title: 'Unbanning Provider Account',
                        icon: "{{ asset('images/preloader.gif') }}",
                        allowOutsideClick: false,
                        closeOnEsc: false,
                        allowEnterKey: false,
                    });
                },
                success: function(data) {
                    swal.close();
                    if (data.status == 1) {
                        swal({
                            title: "UnBan Successful",
                            text: data.msg,
                            icon: "success",
                        });
                        window.setTimeout(function() {
                            location.reload(true);
                        }, 3000);
                    } else {
                        swal({
                            title: "Error!",
                            text: data.msg,
                            icon: "error",
                        });
                        $('button').removeAttr('disabled');
                    }
                },
                error: function(xhr, status, error) {
                    //other stuff
                    swal.close();
                    console.log(xhr.responseText);
                    var errors = $.parseJSON(xhr.responseText);
                    errors = errors.errors;
                    console.log(errors);
                    if (errors != null) {
                        var items = '<ul>';
                        $.each(errors, function(key, value) {
                            items += '<li>' + key + ': ' + value + '</li>';
                        });
                        items += '</ul>';
                        $('#errors').empty();
                        $('#errors').append(items);
                        $('#errors').show();
                    }
                    setTimeout(function() {
                        swal({
                            icon: 'error',
                            title: 'Error',
                            text: xhr.responseJSON.msg,
                            timer: 5000
                        }).then((value) => {}).catch(swal.noop)
                    }, 1000);
                    $('button').removeAttr('disabled');
                }
            });
        });


    });
</script>
<!-- End custom js for this page-->
@endpush

@endsection
