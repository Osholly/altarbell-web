<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Create Admin Provider | Altarbell Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-8 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo">
                                <img src="{{ asset('images/logo.png') }}" alt="logo">
                            </div>
                            <h6 class="font-weight-light">Create a provider linked and accessible right from your admin
                                dashboard.</h6>
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form class="pt-3" method="POST" action="{{ route('admin.providers.store') }}">
                                @csrf
                                <div class="form-row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span class="input-group-text bg-transparent border-right-0">
                                                        <i class="ti-user text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="text" name="first_name"
                                                    class="form-control form-control-lg border-left-0 @error('first_name') is-invalid @enderror"
                                                    placeholder="First Name" value="{{ old('first_name') }}" required
                                                    autocomplete="first_name" autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span class="input-group-text bg-transparent border-right-0">
                                                        <i class="ti-user text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="text"
                                                    class="form-control form-control-lg border-left-0 @error('last_name') is-invalid @enderror"
                                                    placeholder="Last Name" value="{{ old('last_name') }}"
                                                    name="last_name" autocomplete="last_name" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span class="input-group-text bg-transparent border-right-0">
                                                        <i class="ti-user text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="text" name="title"
                                                    class="form-control form-control-lg border-left-0 @error('title') is-invalid @enderror"
                                                    placeholder="Title" value="{{ old('title') }}" required
                                                    autocomplete="title" autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>Organization(Optional)</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span class="input-group-text bg-transparent border-right-0">
                                                        <i class="ti-home  text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="text"
                                                    class="form-control form-control-lg border-left-0 @error('organization') is-invalid @enderror"
                                                    placeholder="Organization" value="{{ old('organization') }}"
                                                    name="organization" autocomplete="organization">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail">Email Address</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span
                                                        class="input-group-text bg-transparent border-right-0 @error('email') is-invalid @enderror">
                                                        <i class="ti-email text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="email"
                                                    class="form-control form-control-lg border-left-0 @error('email') is-invalid @enderror"
                                                    id="exampleInputEmail1" placeholder="Email Address" name="email"
                                                    value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-lg-6'>
                                        <div class="form-group">
                                            <label for="exampleInputPhone">Phone Number</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span
                                                        class="input-group-text bg-transparent border-right-0 @error('phone') is-invalid @enderror">
                                                        <i class="ti-microphone text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="tel"
                                                    class="form-control form-control-lg border-left-0 @error('phone') is-invalid @enderror"
                                                    id="exampleInputPhone" placeholder="Phone Number" name="phone"
                                                    value="{{ old('phone') }}" required autocomplete="phone">
                                            </div>
                                            <small>Kindly use the international format</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="js-example-basic-single w-100 countries order-alpha"
                                                id="countryId" name="country" required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="js-example-basic-single w-100 states order-alpha"
                                                id="stateId" name="state" required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>City</label>
                                            <select class="js-example-basic-single w-100 cities order-alpha" id="cityId"
                                                name="city" required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" placeholder="Your Address" rows="5" name="address"
                                        required>{{ old('address') }}</textarea>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span class="input-group-text bg-transparent border-right-0">
                                                        <i class="ti-lock text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="password"
                                                    class="form-control form-control-lg border-left-0 @error('password') is-invalid @enderror"
                                                    id="exampleInputPassword1" placeholder="Password" name="password"
                                                    required autocomplete="current-password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend bg-transparent">
                                                    <span class="input-group-text bg-transparent border-right-0">
                                                        <i class="ti-lock text-primary"></i>
                                                    </span>
                                                </div>
                                                <input type="password"
                                                    class="form-control form-control-lg border-left-0 @error('confirm_password') is-invalid @enderror"
                                                    id="exampleInputPassword1" placeholder="Confirm Password"
                                                    name="password_confirmation" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3 mb-2">
                                    <button type="submit"
                                        class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">Create
                                        Account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
    <script src="{{ asset('vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('js/select2.js') }}"></script>
    <!-- endinject -->
    <script src="https://geodata.solutions/includes/countrystatecity.js"></script>
</body>

</html>
