@extends('admin.layouts.layout')

@section('title', "$provider->first_name $provider->last_name | AltarBell Provider")

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="border-bottom text-center pb-4">
                                <img src="{{ Storage::url($provider->avatar) }}" alt="profile"
                                    class="img-lg rounded-circle mb-3" />
                                <div class="mb-3">
                                    <h3>{{ $provider->title.' '.$provider->first_name.' '.$provider->last_name }}</h3>
                                </div>
                                <p class="w-75 mx-auto mb-3">{{ $provider->address }}</p>
                            </div>
                            <div class="py-4">
                                <p class="clearfix">
                                    <span class="float-left">
                                        Phone
                                    </span>
                                    <span class="float-right text-muted">
                                        {{ $provider->phone }}
                                    </span>
                                </p>
                                <p class="clearfix">
                                    <span class="float-left">
                                        Mail
                                    </span>
                                    <span class="float-right text-muted">
                                        {{ $provider->email }}
                                    </span>
                                </p>
                                <p class="clearfix">
                                    <span class="float-left">
                                        Organization
                                    </span>
                                    <span class="float-right text-muted">
                                        {{ $provider->organization }}
                                    </span>
                                </p>
                                <p class="clearfix">
                                    <span class="float-left">
                                        Location
                                    </span>
                                    <span class="float-right text-muted">
                                        {{ $provider->country.', '.$provider->state.', '.$provider->city }}
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="profile-feed">
                                @foreach($provider->albums as $album)
                                <div class="d-flex align-items-start profile-feed-item">
                                    <a href="{{ route('admin.providers.albums.show', ['id' => $album->uuid]) }}">
                                        <div class="ml-4">
                                            <h6>
                                                {{ $album->title }}
                                                <small class="ml-4 text-muted"><i
                                                        class="ti-time mr-1"></i>{{ $album->created_at->diffForHumans() }}</small>
                                            </h6>
                                            <img src="{{ Storage::url($album->album_art) }}" alt="sample"
                                                class="rounded mw-100" />
                                            <p class="small text-muted mt-2 mb-0">
                                                <span>
                                                    <i class="ti-heart mr-1"></i>{{ $album->notifications->count() }}
                                                </span>
                                                <span>
                                                    <i class="ti-music-alt mr-1"></i>{{ $album->sounds->count() }}
                                                </span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
