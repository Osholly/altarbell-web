@extends('admin.layouts.layout')

@section('title', 'Manage Adverts')

@section('content')
    <!--********************************** Content body start ***********************************-->
    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Approve and Manage Advert</h4>
                    </div>
                </div>

                <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                    <div class="bootstrap-modal">
                        <!-- Approve Modal -->
                        <div class="modal fade" id="basicModal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Approve Advert</h5> <button type="button" class="close"
                                                                                            data-dismiss="modal"><span>&times;</span> </button>
                                    </div>
                                    <form action="{{ route('admin.adverts.approve') }}" method="POST">
                                        @csrf
                                        <input type="hidden" id="approve_advert_id" name="advert_id" value="">
                                        <div class="modal-body">
                                            <p>Are you sure you want to approve this advert?</p>
                                        </div>
                                        <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Approve Advert</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Decline Modal -->
                        <div class="modal fade" id="editModal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Decline Advert</h5>
                                        <button type="button" class="close"
                                                data-dismiss="modal"><span>&times;</span> </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to decline this advert?</p>
                                    </div>
                                    <form action="{{ route('admin.adverts.decline') }}" method="POST">
                                        @csrf
                                        <div>
                                            <label for="reason">Reason</label>
                                            <textarea class="form-control" name="reason" required id="reason" placeholder="reason"></textarea>
                                        </div>
                                        <input type="hidden" id="decline_advert_id" value="" name="advert_id">
                                        <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Decline Advert</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- View more advert -->
                        <div class="modal fade" id="moreModal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Decline Advert</h5> <button type="button" class="close"
                                                                                            data-dismiss="modal"><span>&times;</span> </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="#">
                                            <div class="form-group"> <label><strong>Advert</strong></label> <input type="text" value="Heavy is the head" class="form-control" required> </div>
                                            <div class="form-group"> <label>Feature</label> <input type="text" class="form-control" value="feature of advert">
                                            </div>
                                            <div class="form-group"> <label>Date Added</label> <input type="text" class="form-control" value="16 April 2021">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Yes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-sm table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Date added</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($adverts as $advert)
                                        <tr>
                                            <th>{{ $loop->iteration }}</th>
                                            <td>{{ Str::headline($advert->title) }}</td>
                                            <td>{{ $advert->category->name }}</td>
                                            <td>{{ $advert->created_at->toDayDateTimeString() }}</td>
                                            <td class="color-primary">
                                                <a href="#" class="badge badge-primary" data-toggle="modal"
                                                                         data-target="#moreModal">View More</a>
                                                <a href="javascript:void()" class="badge badge-danger mr-1 text-white decline" data-toggle="modal"
                                                   data-target="#editModal" data-id="{{ $advert->uuid }}">Decline</a>
                                                <a href="javascript:void()" class="badge badge-success text-white approve" data-toggle="modal"
                                                   data-target="#basicModal" data-id="{{ $advert->uuid }}">Approve</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--********************************** Content body end ***********************************-->
    @endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
    <script>
        $('document').ready(function(){

            $('.decline').click(function () {
                const advert_id = $(this).attr('data-id');

                $('#decline_advert_id').val(advert_id);
            });

            $('.approve').click(function () {
                const advert_id = $(this).attr('data-id');

                $('#approve_advert_id').val(advert_id);

                console.log(advert_id);
                console.log($('#approve_advert_id').val());
            });


            @if(Session::has('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: '{{ session()->get('success') }}'
            })
            @endif

            @if(Session::has('error'))
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: '{{ session()->get('error') }}'
            })
            @endif

        });
    </script>
    <script>
@endpush
