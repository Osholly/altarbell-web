@extends('admin.layouts.layout')

@section('title', 'Album Categories')

@section('content')
@push('styles')

<!-- Plugin css for this page -->
{{--<link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">--}}

<!-- End plugin css for this page -->
@endpush

<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Album Category</h4>
                </div>
            </div>

            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <div class="bootstrap-modal">
                    <!-- Button trigger modal --> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicModal">Add New Category</button> <!-- Modal -->
                    <div class="modal fade" id="basicModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Create New Category</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span> </button>
                                </div>
                                <div class="modal-body">
                                    <form id="create-category-form">
                                        <div id="errors" class="alert alert-danger"></div>
                                        <div class="form-group">
                                            <label><strong>Name</strong></label>
                                            <input type="text" class="form-control" id="name" required>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="create-category-btn">Add New Category</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Album Category</h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span> </button>
                        </div>
                        <div class="modal-body">
                            <form action="#">
                                <div id="update-errors" class="alert alert-danger"></div>
                                <div class="form-group">
                                    <label><strong>Name</strong></label>
                                    <input type="text"  class="form-control" id="update-name" required name="name">
                                    <input type="hidden" id="update-uuid">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="update-category-btn">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if($categories->isNotEmpty())
                            <div class="table-responsive">
                                <table id="example" class="display" style="min-width: 845px">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Name</th>
                                        <th>Album Count</th>
                                        <th>Date Created</th>
                                        <th>Last Updated</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->albums->count() }}</td>
                                            <td>{{ $category->created_at->toDayDateTimeString() }}</td>
                                            <td>{{ $category->updated_at->diffForHumans() }}</td>
                                            <td><a href="javascript:void()" data-toggle="modal" data-target="#editModal"
                                                   class="badge badge-warning text-white edit-category-btn"
                                                   data-id="{{ $category->uuid }}" data-name="{{ $category->name }}">Edit</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
        Content body end
    ***********************************-->

@push('scripts')

<!-- Plugin js for this page-->
<script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('vendors/sweetalert/sweetalert.min.js') }}"></script>
<!-- End plugin js for this page-->

<!-- Custom js for this page-->
<!-- Datatable -->
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins-init/datatables.init.js') }}"></script>

@include('scripts.admin.create-category')
@include('scripts.admin.edit-category')
<!-- End custom js for this page-->
@endpush

@endsection
