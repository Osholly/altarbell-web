<!--**********************************
        Sidebar start
    ***********************************-->
<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            <li><a class="" href="{{ route('admin.home') }}" aria-expanded="false"><i class="icon icon-app-store"></i><span
                        class="nav-text">Dashboard</span></a>
            </li>
            <li><a class="" href="{{ route('admin.categories.index') }}" aria-expanded="false"><i class="fas fa-cog"></i><span
                        class="nav-text">Categories</span></a>
            </li>
            <li><a class="" href="{{ route('admin.providers.index') }}" aria-expanded="false"><i class="fas fa-user"></i><span
                        class="nav-text">Providers</span></a>
            </li>
            <li><a class="" href="{{ route('admin.adverts.index') }}" aria-expanded="false"><i class="fas fa-user"></i><span
                        class="nav-text">Adverts</span></a>
            </li>
            <li><a class="" href="javascript:void()" aria-expanded="false"><i class="far fa-user"></i><span
                        class="nav-text">Admin</span></a>
            </li>

    </div>


</div>
<!--**********************************
        Sidebar end
    ***********************************-->
