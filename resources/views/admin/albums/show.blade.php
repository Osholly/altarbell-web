@extends('admin.layouts.layout')

@section('title', $album->title.' Preview')

@section('content')

@push('styles')

<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/dropify/dropify.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/amplitudejs/amplitudejs.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/fullcalendar/fullcalendar.min.css') }}">
@endpush

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h3>{{ $album->title }}</h3>
                </div>
            </div>
        </div>
    </div>


    <div class="row grid-margin">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="col-lg-12">
                    <h4 class="mt-5">Upcoming Sounds</h4>
                    <div class="mb-3 float-right">
                        <button class="btn btn-success rounded-0" id="add-sound" data-toggle="modal"
                            data-target="#create-sound-modal">Add New Sound</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if(empty($album->sounds))
                            <p class="text-center">No upcoming sounds available for this album.</p>
                            @else
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Play Date</th>
                                            <th>Date Created</th>
                                            <th>Last Updated</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($album->upcoming_sounds as $sound)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sound->title }}</td>
                                            <td>{{ $sound->description }}</td>
                                            <td>{{ $sound->play_date->toDateString() }}</td>
                                            <td>{{ $sound->created_at->toDayDateTimeString() }}</td>
                                            <td>{{ $sound->updated_at->diffForHumans() }}</td>
                                            <td>
                                                <button class="btn btn-outline-primary pull-left view-sound-btn"
                                                    data-id="{{ $sound->uuid }}">View</button>
                                                <button class="btn btn-outline-warning">Edit</button>
                                                <a class="btn btn-outline-danger pull-right"
                                                    href="{{ route('admin.providers.sounds.delete', ['id' => $sound->uuid]) }}">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($album->upcoming_sounds->count() > 0)
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mt-5">Preview Upcoming Sounds</h4>
                            <!-- Blue Playlist Container -->
                            <div id="blue-playlist-container">

                                <!-- Amplitude Player -->
                                <div id="amplitude-player">

                                    <!-- Left Side Player -->
                                    <div id="amplitude-left">
                                        <img data-amplitude-song-info="cover_art_url" class="album-art" />
                                        <div class="amplitude-visualization" id="large-visualization">

                                        </div>
                                        <div id="player-left-bottom">
                                            <div id="time-container">
                                                <span class="current-time">
                                                    <span class="amplitude-current-minutes"></span>:<span
                                                        class="amplitude-current-seconds"></span>
                                                </span>
                                                <div id="progress-container">
                                                    <div class="amplitude-wave-form">

                                                    </div>
                                                    <input type="range" class="amplitude-song-slider" />
                                                    <progress id="song-played-progress"
                                                        class="amplitude-song-played-progress"></progress>
                                                    <progress id="song-buffered-progress"
                                                        class="amplitude-buffered-progress" value="0"></progress>
                                                </div>
                                                <span class="duration">
                                                    <span class="amplitude-duration-minutes"></span>:<span
                                                        class="amplitude-duration-seconds"></span>
                                                </span>
                                            </div>

                                            <div id="control-container">
                                                <div id="repeat-container">
                                                    <div class="amplitude-repeat" id="repeat"></div>
                                                    <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle">
                                                    </div>
                                                </div>

                                                <div id="central-control-container">
                                                    <div id="central-controls">
                                                        <div class="amplitude-prev" id="previous"></div>
                                                        <div class="amplitude-play-pause" id="play-pause"></div>
                                                        <div class="amplitude-next" id="next"></div>
                                                    </div>
                                                </div>

                                                <div id="volume-container">
                                                    <div class="volume-controls">
                                                        <div class="amplitude-mute amplitude-not-muted"></div>
                                                        <input type="range" class="amplitude-volume-slider" />
                                                        <div class="ms-range-fix"></div>
                                                    </div>
                                                    <div class="amplitude-shuffle amplitude-shuffle-off"
                                                        id="shuffle-right"></div>
                                                </div>
                                            </div>

                                            <div id="meta-container">
                                                <span data-amplitude-song-info="name" class="song-name"></span>
                                                <span amplitude-song-info="genre"></span>
                                                <div class="song-artist-album">
                                                    <span data-amplitude-song-info="artist"></span>
                                                    <span data-amplitude-song-info="album"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Left Side Player -->

                                    <!-- Right Side Player -->
                                    <div id="amplitude-right">
                                        @foreach($album->upcoming_sounds as $sound)
                                        <div class="song amplitude-song-container amplitude-play-pause"
                                            data-amplitude-song-index="{{ $loop->index }}">
                                            <div class="song-now-playing-icon-container">
                                                <div class="play-button-container">
                                                </div>
                                                <img class="now-playing"
                                                    src="https://521dimensions.com/img/open-source/amplitudejs/blue-player/now-playing.svg" />
                                            </div>
                                            <div class="song-meta-data">
                                                <span class="song-title">{{ $sound->title }}</span>
                                                <span
                                                    class="song-artist">{{ $sound->album->provider->first_name.' '.$sound->album->provider->last_name }}</span>
                                            </div>
                                            <audio class="manual-audio" id="{{ $sound->uuid }}"
                                                src="{{ Storage::url($sound->path) }}"></audio>
                                            <span class="song-duration">{{ $sound->play_date->format('d/m') }}</span>
                                        </div>
                                        @endforeach
                                    </div>
                                    <!-- End Right Side Player -->
                                </div>
                                <!-- End Amplitdue Player -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('provider.album.calendar')
    @endif


    {{-- Create sound modal --}}
    <div class="modal fade" id="create-sound-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">Add New Sound</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-sound-form">
                        <div id="errors" class="alert alert-danger"></div>
                        <div class="form-group">
                            <label for="album" class="col-form-label">Album</label>
                            <input type="text" value="{{ $album->title }}" readonly class="form-control" id="album"
                                required name="album_id">
                            <input type="hidden" value="{{ $album->uuid }}" class="form-control" id="album-id" required
                                name="album_id">
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-form-label">Sound Title</label>
                            <input type="text" class="form-control" id="title" required name="title">
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-form-label">Sound Description</label>
                            <textarea name="description" id="description" class="form-control" rows="5"
                                placeholder="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="play-date" class="col-form-label">Play Date</label>
                            <div id="datepicker-popup" class="input-group date datepicker">
                                <input type="text" id="play-date"
                                    value="{{ $album->upcoming_sounds->count() > 0 ? $album->next_available_sound_date() : ''}}"
                                    name="play_date" class="form-control" required
                                    {{ $album->upcoming_sounds->count() > 0 ? 'readonly' : ''}}>
                                <span class=" input-group-addon input-group-append border-left">
                                    <span class="ti-calendar input-group-text"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Audio File</label>
                            <input type="file" class="dropify" data-height="250" id="file" name="file" required />
                            <small>Kindly note that supported file formats are MP3, M4A, FLAC, WAV and AAC</small>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="add-sound-btn">Add New Sound</button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')

<!-- Plugin js for this page-->
<script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('vendors/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('vendors/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('vendors/amplitudejs/amplitude.min.js') }}"></script>
<script src="{{ asset('vendors/moment/moment.min.js') }}"></script>
<script src="{{ asset('vendors/fullcalendar/fullcalendar.min.js') }}"></script>

<!-- End plugin js for this page-->

<!-- Custom js for this page-->
<script src="{{ asset('js/formpickers.js') }}"></script>
<script src="{{ asset('js/data-table.js') }}"></script>
<script src="{{ asset('js/dropify.js') }}"></script>
@if($album->upcoming_sounds->count() > 0)
@include('scripts.provider.calendar', ['album' => $album])
@include('scripts.provider.amplitudejs', ['sounds' => $album->upcoming_sounds])
@endif
<script>
    $('#errors').hide();
</script>
@include('scripts.admin.add-sound')
<!-- End custom js for this page-->
@endpush
@endsection
