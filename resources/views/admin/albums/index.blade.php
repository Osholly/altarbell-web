@extends('admin.layouts.layout')

@section('title', 'My Albums')

@section('content')

@push('styles')

<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/dropify/dropify.min.css') }}">

<!-- End plugin css for this page -->
<style>
    .select2-selection__rendered {
        line-height: 31px !important;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .select2-selection__arrow {
        height: 34px !important;
    }

    .select2-selection__rendered {
        margin: -12px;
    }
</style>
@endpush

<div class="content-wrapper">
    <div class="card">
        <div class="col-lg-12">
            <h4 class="card-title mt-5">My Albums</h4>
            <div class="mb-3 float-right">
                <button class="btn btn-success rounded-0" id="create_album" data-toggle="modal"
                    data-target="#create-album-modal" data-whatever="@mdo">Add New Album</button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    @if($albums->isEmpty())
                    <p class="text-center">No albums created yet. Kindly create a new album to get started.</p>
                    @else
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Album Art</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Date Created</th>
                                    <th>Last Updated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($albums as $album)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><img src="{{ Storage::url($album->album_art) }}"></td>
                                    <td>{{ $album->title }}</td>
                                    <td>{{ $album->category->name }}</td>
                                    <td>{{ $album->created_at->toDayDateTimeString() }}</td>
                                    <td>{{ $album->updated_at->diffForHumans() }}</td>
                                    <td>
                                        <button class="btn btn-outline-primary pull-left view-album-btn"
                                            data-id="{{ $album->uuid }}">View</button>
                                        <button class="btn btn-outline-warning">Edit</button>
                                        <button class="btn btn-outline-danger pull-right">Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->

{{-- Create album modal --}}
<div class="modal fade" id="create-album-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Create New Album</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-album-form">
                    <div id="errors" class="alert alert-danger"></div>
                    <div class="form-group">
                        <label for="title" class="col-form-label">Title</label>
                        <input type="text" class="form-control" id="title" required name="title">
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" id="category_id" name="category_id">
                            <option></option>
                            @foreach($categories as $category)
                            <option value="{{ $category->uuid }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Album Art</label>
                        <input type="file" class="dropify" data-height="200" id="album-art" name="album_art" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="create-album-btn">Create Album</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')

<!-- Plugin js for this page-->
<script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('vendors/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('vendors/sweetalert/sweetalert.min.js') }}"></script>
<!-- End plugin js for this page-->

<!-- Custom js for this page-->
<script src="{{ asset('js/data-table.js') }}"></script>
<script src="{{ asset('js/dropify.js') }}"></script>
@include('scripts.admin.create-album')
<!-- End custom js for this page-->
@endpush
@endsection
