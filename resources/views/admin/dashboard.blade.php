@extends('admin.layouts.layout')

@section('title', 'Admin Dashboard')

@section('content')
{{--<div class="content-wrapper">--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12 grid-margin">--}}
{{--            <div class="row">--}}
{{--                <div class="col-12 col-xl-5 mb-4 mb-xl-0">--}}
{{--                    <h4 class="font-weight-bold">Hello,--}}
{{--                        {{ Auth::guard('admin')->user()->title." ".Auth::guard('admin')->user()->first_name }}!--}}
{{--                    </h4>--}}
{{--                    <h4 class="font-weight-normal mb-0">Your Dashboard</h4>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Total Number of Users</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ $users->count() }}</h3>--}}
{{--                        <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Total Active Notifications</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ $notifications->where('active', 1)->count() }}</h3>--}}
{{--                        <i class=" ti-alarm-clock icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <a href="{{ route('admin.categories.index') }}">--}}
{{--                        <p class="card-title text-md-center text-xl-left">Total Number of Categories</p>--}}
{{--                        <div--}}
{{--                            class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                                {{ $categories->count() }}</h3>--}}
{{--                            <i class="ti-settings icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <a href="{{ url('') }}">--}}
{{--                        <p class="card-title text-md-center text-xl-left">Total Number of Albums</p>--}}
{{--                        <div--}}
{{--                            class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                                {{ $albums->count() }}</h3>--}}
{{--                            <i class="ti-book icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Total Number of Sounds</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ $sounds->unique('path')->count() }}</h3>--}}
{{--                        <i class="ti-music-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <a href="{{ route('admin.providers.index') }}">--}}
{{--                    <div class="card-body">--}}
{{--                        <p class="card-title text-md-center text-xl-left">Total Number of Providers</p>--}}
{{--                        <div--}}
{{--                            class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                                {{ $providers->count() }}</h3>--}}
{{--                            <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <p class="card-title text-md-center text-xl-left">Total Number of Admins</p>--}}
{{--                    <div--}}
{{--                        class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">--}}
{{--                        <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">--}}
{{--                            {{ $admins->count() }}</h3>--}}
{{--                        <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- content-wrapper ends -->

<!--**********************************
        Content body start
    ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Hello, NQB8!</h4>
                    <p class="mb-0">Your dashboard</p>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Number of Users </div>
                            <div class="stat-digit">{{ $users->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Active Notifications</div>
                            <div class="stat-digit">{{ $notifications->where('active', 1)->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Number of Categories </div>
                            <div class="stat-digit">{{ $categories->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Number of Albums </div>
                            <div class="stat-digit">{{ $albums->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Number of Songs </div>
                            <div class="stat-digit">{{ $sounds->unique('path')->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Number of Providers </div>
                            <div class="stat-digit">{{ $providers->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="stat-widget-two card-body">
                        <div class="stat-content">
                            <div class="stat-text">Total Number of Admins </div>
                            <div class="stat-digit">{{ $admins->count() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<!--**********************************
        Content body end
    ***********************************-->
@endsection
