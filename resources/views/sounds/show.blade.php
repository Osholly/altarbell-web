<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:locale" content="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta property="og:site_name" content="AltarBell">
    <meta property="og:title" content="{{ $sound->title }}" />
    <meta property="og:description" content="{{ $sound->description }}" />
    <meta property="og:image" itemprop="image" content="{{ Storage::url($sound->album->album_art) }}">
    <meta property="og:type" content="music.song" />
    <meta property="og:updated_time" content="1440432930" />
    <meta property="og:url" content="{{ url('sounds/'.$sound->uuid) }}" />


    <title>{{ $sound->title }} | {{ config('app.name')." Sound" }}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('css/amplitudejs/amplitudejs.css') }}">
</head>

<body>
    <div class="container-scroller">

        <div class="container-fluid page-body-wrapper">

            <div class="main-panel">

                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h3>{{ $sound->title }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <!-- Blue Playlist Container -->
                                    <div id="blue-playlist-container">

                                        <!-- Amplitude Player -->
                                        <div id="amplitude-player">

                                            <!-- Left Side Player -->
                                            <div id="amplitude-left">
                                                <img data-amplitude-song-info="cover_art_url" class="album-art" />
                                                <div class="amplitude-visualization" id="large-visualization">

                                                </div>
                                                <div id="player-left-bottom">
                                                    <div id="time-container">
                                                        <span class="current-time">
                                                            <span class="amplitude-current-minutes"></span>:<span
                                                                class="amplitude-current-seconds"></span>
                                                        </span>
                                                        <div id="progress-container">
                                                            <div class="amplitude-wave-form">

                                                            </div>
                                                            <input type="range" class="amplitude-song-slider" />
                                                            <progress id="song-played-progress"
                                                                class="amplitude-song-played-progress"></progress>
                                                            <progress id="song-buffered-progress"
                                                                class="amplitude-buffered-progress"
                                                                value="0"></progress>
                                                        </div>
                                                        <span class="duration">
                                                            <span class="amplitude-duration-minutes"></span>:<span
                                                                class="amplitude-duration-seconds"></span>
                                                        </span>
                                                    </div>

                                                    <div id="control-container">
                                                        <div id="repeat-container">
                                                            <div class="amplitude-repeat" id="repeat"></div>
                                                            <div class="amplitude-shuffle amplitude-shuffle-off"
                                                                id="shuffle">
                                                            </div>
                                                        </div>

                                                        <div id="central-control-container">
                                                            <div id="central-controls">
                                                                <div class="amplitude-prev" id="previous"></div>
                                                                <div class="amplitude-play-pause" id="play-pause"></div>
                                                                <div class="amplitude-next" id="next"></div>
                                                            </div>
                                                        </div>

                                                        <div id="volume-container">
                                                            <div class="volume-controls">
                                                                <div class="amplitude-mute amplitude-not-muted"></div>
                                                                <input type="range" class="amplitude-volume-slider" />
                                                                <div class="ms-range-fix"></div>
                                                            </div>
                                                            <div class="amplitude-shuffle amplitude-shuffle-off"
                                                                id="shuffle-right">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="meta-container">
                                                        <span data-amplitude-song-info="name" class="song-name"></span>
                                                        <span amplitude-song-info="genre"></span>
                                                        <div class="song-artist-album">
                                                            <span data-amplitude-song-info="artist"></span>
                                                            <span data-amplitude-song-info="album"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Left Side Player -->
                                            <div class="col-md-6">
                                                <h3 class="text-center">Sound Info</h3>
                                                <div class="form-group text-center">
                                                    <label class='font-weight-bold'>Title</label>
                                                    <p>{{ $sound->title }}</p>
                                                </div>
                                                <div class="form-group text-center">
                                                    <label class='font-weight-bold'>Album</label>
                                                    <p>{{ $sound->album->title }}</p>
                                                </div>
                                                <div class="form-group text-center">
                                                    <label class='font-weight-bold'>Description</label>
                                                    <p>{{ $sound->description }}</p>
                                                </div>
                                                <div class="form-group text-center">
                                                    <label class='font-weight-bold'>Date Created</label>
                                                    <p>{{ $sound->created_at->toDayDateTimeString() }}</p>
                                                </div>
                                                <div class="form-group text-center">
                                                    <label class='font-weight-bold'>Last Updated</label>
                                                    <p>{{ $sound->updated_at->diffForHumans() }}</p>
                                                </div>
                                                <div class="form-group text-center">
                                                    <label class='font-weight-bold'>Play Date</label>
                                                    <p>
                                                        {{ $sound->play_date->format('l, jS \of F, Y') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Amplitdue Player -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('vendors/amplitudejs/amplitude.min.js') }}"></script>
    @include('scripts.provider.sound-preview')
    <!-- End custom js for this page-->
</body>

</html>
