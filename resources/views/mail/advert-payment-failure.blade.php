@component('mail::message')
# Introduction

Hello, {{ $advert->provider->first_name." ".$advert->provider->last_name }} this is to inform you that the payment has failed for your
approved advert campaign titled "{{ $advert->title }}". Kindly login to your account and update your payment method for your campaign to go live.

@component('mail::button', ['url' => route('provider.auth.login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
