@component('mail::message')
# Introduction

Hello {{ $advert->provider->first_name." ".$advert->provider->first_name }}, this is to notify you that we have successfully payment for your
advert campaign titled "{{ $advert->title }}" with us and the campaign has been activated.

{{--@component('mail::button', ['url' => route('provider.auth.login')])--}}
{{--Login--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
