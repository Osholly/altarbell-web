<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->uuid('category_id');
            $table->uuid('user_id')->nullable();
            $table->uuid('provider_id')->nullable();
            $table->string('title');
            $table->string('content');
            $table->string('url');
            $table->string('image');
            $table->date('start_date');
            $table->integer('duration')->comment('In days');
            $table->boolean('status')->nullable();
            $table->expires();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
