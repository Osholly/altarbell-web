<?php

namespace Database\Seeders;

use App\Models\Advert;
use App\Models\Album;
use App\Models\Admin;
use App\Models\Sound;
use App\Models\Category;
use Database\Factories\AdvertFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdvertSeeder::class,
            AlbumSeeder::class,
            SoundSeeder::class
        ]);
    }
}