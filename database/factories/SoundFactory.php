<?php

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\Album;
use App\Models\Sound;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class SoundFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sound::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $albums = Album::all();

        return [
            //
            'album_id' => $albums->random()->uuid,
            'title' => Str::random(),
            'description' => $this->faker->text,
            'path' =>   $this->faker->url,
            'play_date' => Carbon::now()->addDays(rand(1, 2))->toDateString()
        ];
    }
}
