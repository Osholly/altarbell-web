<?php

namespace Database\Factories;

use App\Models\Advert;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Str;

class AdvertFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Advert::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => Category::pluck('uuid')->random(),
            'title' => $this->faker->words(mt_rand(3,10), true),
            'content' => $this->faker->sentence(10),
            'url' => $this->faker->url(),
            'image' => $this->faker->imageUrl(),
            'target' => mt_rand(30, 1000),
            'start_date' => $this->faker->dateTimeBetween(now()->toDateString(), now()->addRealWeeks(100)),
            'duration' => mt_rand(5, 15)];
    }

    public function canBelongToUser()
    {
        return $this->state( new Sequence(
            ['user_id' => null],
            ['user_id' => Str::uuid()]
        ));
    }

    public function canBelongToProvider()
    {
        return $this->state(new Sequence(
            ['provider_id' => null],
            ['provider_id' => Str::uuid()]
        ));
    }

    public function canBeApprovedOrRejectedOrPending()
    {
        return $this->state(new Sequence(
            ['status' => null],
            ['status' => 0],
            ['status' => 1]
        ));
    }
}
