<?php

namespace Database\Factories;

use App\Models\Album;
use App\Models\Category;
use App\Models\Provider;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlbumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Album::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'provider_id' => Provider::all()->random()->uuid,
            'title' => Str::random(),
            'category_id' => Category::all()->random()->uuid,
            'album_art' => $this->faker->imageUrl('800', '600', 'Album')
        ];
    }
}