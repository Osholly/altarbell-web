<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Album;
use Illuminate\Console\Command;
use App\Mail\SoundUploadReminder;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Builder;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind Providers with albums running out of content.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $albums = Album::whereDoesntHave('upcoming_sounds', function (Builder $query) {
            $query->whereDate('play_date', '>=', Carbon::today()->addDay(5)->toDateString());
        })->get();

        foreach ($albums as $album) {
            # code...

            $email = $album->provider->email;

            Mail::to($email)->send(new SoundUploadReminder($album));
        }
    }
}