<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Album;
use App\Models\Sound;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;

class ReplicateSounds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'replicate:sounds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replicates sounds from each album without fresh sounds with the previous sounds from a specified number of days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $albums = Album::doesntHave('sounds.upcoming')->get();

        if ($albums != null) {
            # code...

            foreach ($albums as $album) {
                # code...

                $sounds = Sound::where('album_id', $album->uuid)->orderBy('play_date', 'desc');

                $lastMonthCount = $sounds->lastMonth()->get()->count();
                $totalCount = $sounds->get()->count();

                $sounds->when($lastMonthCount >= 31, function ($query) {
                    return $query->orderBy('play_date', 'desc')->lastMonth();
                });

                $sounds->when($lastMonthCount < 31 && $totalCount >= 31, function ($query) {
                    return $query->orderBy('play_date', 'desc')->take(31);
                });

                $sounds = $sounds->get();

                if ($sounds != null) {
                    # code...

                    $count = 1;

                    foreach ($sounds as $sound) {
                        # code...

                        $date = Carbon::today()->addDays($count)->toDateString();

                        if ($album->todaySound($date)) {
                            # code...
                            continue;
                        }

                        $sound->update([
                            'play_date' => $date
                        ]);

                        $count++;
                    }
                }
            }
        }
    }
}