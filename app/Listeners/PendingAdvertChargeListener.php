<?php

namespace App\Listeners;

use App\Events\AdvertPaymentFailureEvent;
use App\Events\AdvertPaymentSuccessfulEvent;
use App\Models\Advert;
use App\Models\Transaction;
use App\Traits\PaystackTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PendingAdvertChargeListener
{
    use PaystackTrait;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $pending_adverts = Advert::where('provider_id', $event->paymentMethod->provider_id)->where('status', 2)->get();

        $data = [
            'authorization_code' => $event->payment_method->authorization_code,
            'email' => $event->payment_method->email,
            'amount' => 10000
        ];

        foreach($pending_adverts as $advert){

            $payment = $this->chargeAuthorization($data);

            if ($payment != null){

                $advert->update(['status' => 1]);

                $transaction = Transaction::create([
                    'provider_id' => $advert->provider_id,
                    'amount' => $payment['data']['amount'],
                    'advert_id' => $advert->uuid,
                    'description' => "Payment for advert ".$advert->title,
                    'status' => 'success'
                ]);

                event(new AdvertPaymentSuccessfulEvent($advert));
            } else {

                event(new AdvertPaymentFailureEvent($event->advert));
            }

        }
    }
}
