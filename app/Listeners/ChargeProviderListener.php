<?php

namespace App\Listeners;

use App\Events\AdvertPaymentFailureEvent;
use App\Events\AdvertPaymentSuccessfulEvent;
use App\Mail\AdvertPaymentError;
use App\Models\Transaction;
use App\Traits\PaystackTrait;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class ChargeProviderListener implements ShouldQueue
{
    use PaystackTrait;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        try {
            $transaction = DB::transaction(function () use ($event){

                $payment_method = $event->advert->provider->payment_method;

                $data = [
                  'amount' => 10000,
                  'email' => $payment_method->email,
                  'authorization_code' => $payment_method->authorization_code
                ];

                $payment = $this->chargeAuthorization($data);

                throw_if(!$payment, new Exception("Error charging provider for advert"));

                if ($payment['data']['status'] == "success") {

                    $event->advert->update([
                        'status' => 1
                    ]);

                    $transaction = Transaction::create([
                        'provider_id' => $event->advert->provider_id,
                        'amount' => $payment['data']['amount'],
                        'advert_id' => $event->advert->uuid,
                        'description' => "Payment for advert ".$event->advert->title,
                        'status' => 'success'
                    ]);

                    event(new AdvertPaymentSuccessfulEvent($event->advert));
                } else {

                    event(new AdvertPaymentFailureEvent($event->advert));

                    $event->advert->update([
                        'status' => 2
                    ]);

                }

            });
        } catch (Throwable $exception) {
            report($exception);

            Log::error($exception->getMessage());

        }
    }
}
