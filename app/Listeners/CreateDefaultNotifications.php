<?php

namespace App\Listeners;

use App\Models\Album;
use App\Models\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateDefaultNotifications
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $user = $event->user;

        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        $days = json_encode($days);

        $names = ['Wakeup', 'Devotion', 'Bed Time'];

        $times = ['06:00:00', '07:00:00', '21:00:00'];

        $count = count($names);

        for ($i = 0; $i < $count; $i++) {
            # code...

            $data = [
                'user_id' => $user->uuid,
                'album_id' => '',
                'days' => $days,
                'name' => $names[$i],
                'time' => $times[$i],
                'is_repeat' => true,
                'snooze_time' => 5,
                'active' => false
            ];

            $notification = Notification::create($data);
        }
    }
}