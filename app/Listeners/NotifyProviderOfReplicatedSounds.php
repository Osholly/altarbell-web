<?php

namespace App\Listeners;

use App\Mail\AlbumSoundsReplicated;
use Illuminate\Support\Facades\Mail;


class NotifyProviderOfReplicatedSounds
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $album = $event->album;

        Mail::to($album->provider->email)->send(new AlbumSoundsReplicated($album));
    }
}