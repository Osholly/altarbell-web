<?php

namespace App\Listeners;

use App\Mail\AdvertPaymentSuccessful;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendPaymentSuccessNotification
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $advert = $event->advert;

        Mail::to($advert->provider->email)->send(new AdvertPaymentSuccessful($advert));
    }
}
