<?php

namespace App\Traits;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

trait PaystackTrait
{

    public function initializeTransaction(array $data)
    {
        try {
            $transaction = Http::withToken(config('services.paystack.secret'))
                ->acceptJson()
                ->retry(3, 100)
                ->timeout(15)
                ->post(config('services.paystack.url')."/transaction/initialize", $data)
                ->throw();

        } catch(RequestException | Throwable $exception) {
            report($exception);

            Log::debug($exception);

            return null;
        }

        return $transaction->json();
    }

    public function verifyTransaction(string $reference)
    {
        try {
            $transaction = Http::withToken(config('services.paystack.secret'))
                ->acceptJson()
                ->retry(3, 100)
                ->timeout(15)
                ->get(config('services.paystack.url')."/transaction/verify/".$reference)
                ->throw();

        } catch(Throwable $exception){
            report($exception);

            Log::error($exception->getMessage());

            return null;
        }

        return $transaction->json();
    }

    public function chargeAuthorization(array $data)
    {
        try {
            $transaction = Http::withToken(config('services.paystack.secret'))
                ->acceptJson()
                ->retry(3, 100)
                ->timeout(15)
                ->post(config('services.paystack.url')."/transaction/charge_authorization", $data)
                ->throw();

        } catch(RequestException | Throwable $exception) {
            report($exception);

            Log::debug($exception);

            return null;
        }

        return $transaction->json();
    }

}
