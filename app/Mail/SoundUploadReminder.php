<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SoundUploadReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $album;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($album)
    {
        //
        $this->album = $album;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('provider.emails.sound-upload-reminder');
    }
}