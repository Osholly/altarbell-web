<?php

namespace App\Http\Controllers\Provider\PaymentMethod;

use App\Http\Controllers\Controller;
use App\Models\Advert;
use App\Traits\PaystackTrait;
use App\Traits\ResponseTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class PaymentMethodController extends Controller
{
    use PaystackTrait, ResponseTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payment_method = request()->user('provider')->payment_method;

        return view('provider.payment-methods.index', compact('payment_method'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $link = '';

        try {
            $transaction = DB::transaction(function() use (&$link){

                $payment_info = [
                    'email' => request()->user('provider')->email,
                    'amount' => 10000,
                    'metadata' => [
                        'provider' => request()->user('provider')->only(['uuid', 'first_name', 'last_name', 'email']),
                    ],
                    'callback_url' => route('provider.transactions.verify')
                ];

                $payment = $this->initializeTransaction($payment_info);

                throw_if(!$payment, new Exception("Error generating payment info"));

                $link = $payment['data']['authorization_url'];
            });
        } catch(Throwable $exception) {
            report($exception);

            return redirect()->back()->with('error', $exception->getMessage());
        }

        return redirect()->back()->with('success', "Transaction initiated successfully. You will now be redirected shortly.")->with('link', $link);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
