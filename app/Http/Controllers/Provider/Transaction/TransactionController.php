<?php

namespace App\Http\Controllers\Provider\Transaction;

use App\Events\PaymentMethodUpdatedEvent;
use App\Http\Controllers\Controller;
use App\Models\Advert;
use App\Models\PaymentMethod;
use App\Models\Transaction;
use App\Traits\PaystackTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TransactionController extends Controller
{
    use PaystackTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = request()->user('provider')->transactions;

        return view('provider.transactions.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify()
    {
        $reference = request()->query('reference');

        try {
            $payment = $this->verifyTransaction($reference);

            if (!$payment){
                return redirect()->route('provider.payment-methods.index')->with('error', 'Error verifying transaction.');
            }

            if ($payment['data']['status'] == 'failed'){

                return redirect()->route('provider.payment-methods.index')->with('error', 'Transaction failed. Kindly fund your card and try again.');
            }

            $transaction = DB::transaction(function() use($payment) {

                if (session()->has('advert')){
                    $advert = Advert::create(session()->get('advert'));
                    session()->forget('advert');
                }

                $data = [
//                  'user_id' => $payment['user_id']
                  'provider_id' => $payment['data']['metadata']['provider']['uuid'],
                    'advert_id' => null,
                    'amount' => $payment['data']['amount']/100,
                    'status' => $payment['data']['status'],
                    'description' => "Added a new payment method"
                ];

                $transaction = Transaction::create($data);

//                return $transaction;
                $data = array_filter($payment['data']['authorization'] +
                    ['provider_id' => $payment['data']['metadata']['provider']['uuid'], 'email' => $payment['data']['metadata']['provider']['email']]);

                $paymentMethod = PaymentMethod::updateOrCreate(['provider_id' => $payment['data']['metadata']['provider']['uuid']], $data);

                if (!$paymentMethod->wasRecentlyCreated){
                    event(new PaymentMethodUpdatedEvent($paymentMethod));
                }
            });

        } catch (Throwable $exception){
            report($exception);

            Log::error($exception->getMessage());

            return redirect()->route('provider.payment-methods.index')->with('error', $exception->getMessage());
        }

        return redirect()->route('provider.payment-methods.index')->with('success', 'Payment Method saved successfully.');
    }
}
