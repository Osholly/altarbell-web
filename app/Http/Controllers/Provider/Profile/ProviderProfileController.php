<?php

namespace App\Http\Controllers\Provider\Profile;

use App\Models\Provider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdateProviderProfileRequest;

class ProviderProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $provider = Provider::findByUUID($id);

        if ($provider == null) {
            # code...

            abort(404, 'Requested page not found.');
        }

        if (Auth::guard('provider')->user()->uuid != $id) {
            # code...

            abort(403, 'You are unauthorized to view this page.');
        }

        return view('provider.profile.show', ['provider' => $provider]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $provider = Provider::findByUUID($id);

        if ($provider == null) {
            # code...

            abort(404, 'Requested page not found.');
        }

        if (Auth::guard('provider')->user()->uuid != $id) {
            # code...

            abort(401, 'You are unauthorized to view this page.');
        }

        return view('provider.profile.edit', ['provider' => $provider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProviderProfileRequest $request, $id)
    {
        //
        // dd($request->validated());

        $provider = Provider::where('uuid', $id)->first();;

        if ($provider == null) {
            # code...

            abort(404, 'Provider account not found');
        }

        $data = [];

        if ($request->has('avatar')) {
            $extension = $request->file('avatar')->extension();
            $avatar = $request->file('avatar')->storeAs('Providers/Avatars', uniqid() . '.' . $extension, 'public');

            $request->avatar = $avatar;

            $data = [
                'avatar' => $avatar
            ];
        }

        $update = $provider->update(array_filter($data + $request->only([
            'first_name', 'last_name', 'email', 'country_code', 'phone', 'title', 'organization', 'country', 'state', 'city', 'address'
        ])));

        if ($update == false) {
            # code...

            return redirect()->back()->with('fail', 'Error Updating Profile Information')->withInput();
        }

        return redirect()->back()->with('success', 'Profile Information Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}