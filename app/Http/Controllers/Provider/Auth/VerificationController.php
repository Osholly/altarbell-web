<?php

namespace App\Http\Controllers\Provider\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Access\AuthorizationException;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | provider that recently registered with the application. Emails may also
    | be re-sent if the provider didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect providers after verification.
     *
     * @var string
     */
    protected $redirectTo = '/provider';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('provider.auth');
        $this->middleware('signed')->only('provider.verify');
        $this->middleware('throttle:6,1')->only('provider.verify', 'resend');
    }

    /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return $request->user('provider')->hasVerifiedEmail()
            ? redirect($this->redirectPath())
            : view('provider.auth.verify');
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        if (!hash_equals((string) $request->route('id'), (string) $request->user('provider')->getKey())) {
            throw new AuthorizationException;
        }

        if (!hash_equals((string) $request->route('hash'), sha1($request->user('provider')->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if ($request->user('provider')->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        if ($request->user('provider')->markEmailAsVerified()) {
            event(new Verified($request->user('provider')));
        }

        return redirect($this->redirectPath())->with('verified', true);
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user('provider')->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        $request->user('provider')->sendEmailVerificationNotification();

        return back()->with('resent', true);
    }
}