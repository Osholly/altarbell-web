<?php

namespace App\Http\Controllers\Provider\Album;

use App\Models\Album;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests\UpdateAlbumRequest;
use App\Http\Requests\CreateNewAlbumRequest;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $provider = Auth::guard('provider')->user();

        $albums = Album::with('upcoming_sounds')->where('provider_id', $provider->uuid)->paginate(20);

        $categories = Category::all();

        return view('provider.album.index', ['provider' => $provider, 'albums' => $albums, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewAlbumRequest $request)
    {
        //
        $title = $request->title;
        $category_id = $request->category_id;
        $extension = $request->file('album_art')->extension();
        $album_art = $request->file('album_art')->storeAs('Album Arts', uniqid() . '.' . $extension, 'public');

        $album = Album::create([
            'title' => $title,
            'category_id' => $category_id,
            'provider_id' => Auth::guard('provider')->user()->uuid,
            'album_art' => $album_art
        ]);

        if ($album == false) {
            # code...

            $response = [
                'status' => 0,
                'msg' => 'Error creating new album.'
            ];

            return response()->json($response, 400);
        }

        $response = [
            'status' => 1,
            'msg' => 'New Album Successfully created.',
            'url' => route('provider.albums.show', ['id' => $album->uuid])
        ];

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $album = Album::where('uuid', $id)->first();

        if ($album == null) {
            # code...\

            abort(404, 'Requested Album Not Found');
        }

        return view('provider.album.show', ['album' => $album]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAlbumRequest $request, $id)
    {
        //

        $album = Album::findByUuid($id);

        if ($album == null) {
            # code...
            $response = [
                'status' => 0,
                'msg' => 'Requested album not found.'
            ];

            return response()->json($response, 404);
        }

        $data = [];

        if ($request->has('album_art')) {
            # code...

            $old_album_art = $album->album_art;
            $extension = $request->file('album_art')->extension();
            $data['album_art'] = $request->file('album_art')->storeAs('Album Arts', uniqid() . '.' . $extension, 'public');
        }

        $update = $album->update(array_filter(
            $request->only(['title', 'category_id']) + $data
        ));

        if ($request->has('album_art')) {
            # code...

            File::delete($old_album_art);
        }

        $response = [
            'status' => 1,
            'msg' => 'New Album Successfully created.'
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
