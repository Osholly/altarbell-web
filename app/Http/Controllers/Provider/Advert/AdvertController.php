<?php

namespace App\Http\Controllers\Provider\Advert;

use App\Http\Controllers\Controller;
use App\Models\Advert;
use App\Models\Category;
use App\Traits\PaystackTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Throwable;

class AdvertController extends Controller
{
    use PaystackTrait, ResponseTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adverts = Advert::where('status', '!=', 1)
            ->where('provider_id', request()->user('provider')->uuid)
            ->get();

        return view('provider.advert.index', compact('adverts'));
    }

    public function running()
    {
        $adverts = Advert::where('status', 1)
            ->running()
            ->where('provider_id', request()->user('provider')->uuid)
            ->get();

        return view('provider.advert.running', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('provider.advert.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->has('image')){
            $extension = $request->file('image')->extension();
            $image = $request->file('image')->storeAs('Adverts/Images', uniqid() . '.' . $extension, 'public');

            $data['image'] = $image;
        }


        $data['provider_id'] = request()->user('provider')->uuid;

        if(request()->user('provider')->payment_method == null){
            Session::put('advert', $data);

            return redirect()->route('provider.payment-methods.index')->with('info', "Kindly add a payment method to continue");
        }

        $link = '';

        try {
            $advert = DB::transaction(function() use ($data){
                $advert = Advert::create($data);
            });
        } catch(Throwable $exception) {
            report($exception);

            return redirect()->back()->with('error', $exception->getMessage());
        }

        return redirect()->back()->with('success', "Advert application saved successfully. You will be notified once there is an update.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
