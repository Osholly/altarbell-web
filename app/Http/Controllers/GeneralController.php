<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\Album;
use App\Models\Sound;
use App\Models\Category;
use App\Models\Provider;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\AlbumResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Resources\ProviderResourceCollection;

class GeneralController extends Controller
{
    use ResponseTrait;
    //
    public function fetch_categories()
    {
        $categories = Category::all();

        $response = [
            'status' => 'success',
            'message' => 'Album Categories Successfully Fetched',
            'categories' => $categories
        ];

        return response()->json($response);
    }

    public function filter_albums(Request $request)
    {
        $category_id = $request->query('category_id');
        $title = $request->query('title');
        $name = $request->query('provider_name');

        $albums = Album::query();

        $albums->when($name != null, function ($q) use ($name) {
            return $q->whereHas('provider', function ($query) use ($name) {
                return $query->where(DB::raw('concat("first_name", "last_name")'), 'like', "%{$name}%")
                    ->orWhere('first_name', 'like', "%{$name}%")
                    ->orWhere('last_name', 'like', "%{$name}%");
            });
        });

        $albums->when($category_id != null, function ($q) use ($category_id) {
            return $q->where('category_id', $category_id);
        });

        $albums->when($title != null, function ($q) use ($title) {
            return $q->where('title', 'like', '%' . $title . '%');
        });

        $albums = $albums->whereHas('sounds')->paginate(25);

        return new AlbumResourceCollection($albums);
    }

    public function search(Request $request)
    {
        $string = $request->query('param');

        $result = (object) [
            'albums' => Album::search($string)->paginate(25),
            'providers' => Provider::search($string)->paginate(25)
        ];

        return response()->json($result);
    }

    public function sound($id)
    {
        $sound = Sound::findByUUID($id);

        if (!$sound) {
            abort(404, 'Requested sound not found');
        }

        return view('sounds.show', ['sound' => $sound]);
    }

    public function view_advert()
    {
        $advert = Advert::where('uuid', request()->advert_id)->first();

        if(!$advert){
            return $this->error("Requested advert not found", 404);
        }

        $update = $advert->increment('views');

        return $this->success([], "Advert views incremented successfully");
    }

    public function unview_advert()
    {
        $advert = Advert::where('uuid', request()->advert_id)->first();

        if(!$advert){
            return $this->error("Requested advert not found", 404);
        }

        $update = $advert->decrement('views');

        return $this->success([], "Advert views decremented successfully");
    }
}
