<?php

namespace App\Http\Controllers\Admin\Advert;

use App\Events\AdvertApprovedEvent;
use App\Http\Controllers\Controller;
use App\Models\Advert;
use Illuminate\Http\Request;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adverts = Advert::with(['provider', 'user', 'category'])
            ->doesntHave('transaction')
            ->pending()
            ->get();

        return view('admin.adverts.index', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advert = Advert::findByUuid($id);

        abort_if(!$advert, 404, "Requested advert not found");

        $update = $advert->update($request->validated());

        return redirect()->back()->with('success', "Advert updated successfully.");
    }

    public function approve(Request $request)
    {
        $advert = Advert::with('provider')->where('uuid', $request->advert_id)->first();

        if (!$advert){
            return redirect()->back()->with('error', 'Requested advert not found.');
        }

        $update = $advert->update(['status' => 2]);

        AdvertApprovedEvent::dispatch($advert);

        return redirect()->back()->with('success', 'Advert Approved successfully.');
    }

    public function decline(Request $request)
    {
        $data = $request->only(['reason']) + [
            'status' => 0
            ];

        $advert = Advert::with('provider')->where('uuid', $request->advert_id)->first();

        if (!$advert){
            return redirect()->back()->with('error', 'Requested advert not found.');
        }

        $update = $advert->update($data);

        return redirect()->back()->with('success', 'Advert Declined successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
