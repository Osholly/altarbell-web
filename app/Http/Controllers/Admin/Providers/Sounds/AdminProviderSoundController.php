<?php

namespace App\Http\Controllers\Admin\Providers\Sounds;

use Carbon\Carbon;
use App\Models\Sound;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminProviderSoundUploadRequest;

class AdminProviderSoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminProviderSoundUploadRequest $request)
    {
        //
        $date_occupied = Sound::where('album_id', $request->album_id)->where('play_date', Carbon::parse($request->play_date)->toDateString())->exists();

        if ($date_occupied != false) {
            # code...

            $response = [
                'status' => '0',
                'msg' => 'The album already has a sound for the specified date.'
            ];

            return response()->json($response, 200);
        }

        $extension = $request->file('file')->extension();
        $path = $request->file('file')->storeAs('Sounds', uniqid() . '.' . $extension, 'public');
        $title = $request->title;
        $description = $request->description;
        $album_id = $request->album_id;
        $play_date = $request->play_date;

        $sound = Sound::create([
            'path' => $path,
            'title' => $title,
            'play_date' => Carbon::parse($play_date)->format('Y-m-d'),
            'description' => $description,
            'album_id' => $album_id
        ]);

        if ($sound == false) {
            # code...

            $response = [
                'status' => '0',
                'msg' => 'Error Adding New Sound'
            ];

            return response()->json($response, 200);
        }

        $response = [
            'status' => '1',
            'msg' => 'New Sound Successfully added to album'
        ];

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sound = Sound::findByUUID($id);

        if ($sound == null) {
            # code...
            abort(404, 'Requested Sound not found');
        }

        if ($sound->delete()) {
            # code...

            return redirect()->back()->with('success', 'Requested sound successfully deleted.');
        }

        return redirect()->back()->with('error', 'Requested sound successfully deleted.');
    }
}