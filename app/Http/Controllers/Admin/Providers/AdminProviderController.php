<?php

namespace App\Http\Controllers\Admin\Providers;

use Carbon\Carbon;
use App\Models\Provider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\BanProviderRequest;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Http\Requests\CreateAdminProviderRequest;

class AdminProviderController extends Controller
{
    use VerifiesEmails;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $providers = Provider::withBanned()->get();

        return view('admin.providers.index', ['providers' => $providers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::guard('admin')->user()->provider != null) {
            # code...
            abort(403, 'Access restricted');
        }

        return view('admin.providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdminProviderRequest $request)
    {
        //
        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'title' => $request->title,
            'organization' => $request->organization,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->state,
            'is_admin' => true,
            'admin_id' => Auth::guard('admin')->user()->uuid
        ];

        $provider = Provider::create($data);

        $provider->markEmailAsVerified();

        if ($provider == false) {
            # code...

            return redirect()->back()->with('fail', 'Error creating provider account. Please try again.');
        }

        return redirect('/admin/providers/')->with('success', 'Admin Provider account Successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::withBanned()->with('albums')->where('uuid', $id)->first();

        if ($provider == null) {
            # code...
            abort(404, 'Requested Provider Not Found');
        }

        return view('admin.providers.show', compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function ban(BanProviderRequest $request, $id)
    {
        $provider = Provider::withBanned()->where('uuid', $id)->first();

        if ($provider->isBanned()) {
            # code...

            $response = [
                'status' => 0,
                'msg' => 'Provider has been banned already.'
            ];

            return response()->json($response, 400);
        }

        if ($provider->ban($request->validated())) {
            # code...

            $response = [
                'status' => 1,
                'msg' => 'Provider Successfully banned.'
            ];

            return response()->json($response);
        }

        $response = [
            'status' => 0,
            'msg' => 'Error banning provider account.'
        ];

        return response()->json($response, 400);
    }

    public function unban($id)
    {
        $provider = Provider::withBanned()->where('uuid', $id)->first();

        if ($provider->isNotBanned()) {
            # code...

            $response = [
                'status' => 0,
                'msg' => 'Provider not previously banned.'
            ];

            return response()->json($response, 400);
        }

        $provider->unban();

        $response = [
            'status' => 1,
            'msg' => 'Provider Successfully Unbanned.'
        ];

        return response()->json($response);
    }
}