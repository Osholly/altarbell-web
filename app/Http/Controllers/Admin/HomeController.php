<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Admin;
use App\Models\Album;
use App\Models\Sound;
use App\Models\Category;
use App\Models\Provider;
use App\Models\Notification;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Show the Admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard', [
            'albums' => Album::all(),
            'providers' => Provider::all(),
            'categories' => Category::all(),
            'sounds' => Sound::all(),
            'users' => User::all(),
            'notifications' => Notification::all(),
            'admins' => Admin::all()
        ]);
    }
}