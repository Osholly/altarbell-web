<?php

namespace App\Http\Controllers\API\v1\User\Auth;

use Carbon\Carbon;
use App\Models\User;
use App\Models\SocialLogin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\Password;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Requests\SocialLoginRequest;
use App\Http\Resources\SocialLoginResource;
use App\Http\Requests\UserRegistrationRequest;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\UserResetPasswordRequest;
use App\Http\Requests\SocialAuthProviderRequest;
use App\Http\Requests\UserForgotPasswordRequest;

class UserAuthController extends Controller
{
    public function register(UserRegistrationRequest $request)
    {
        //
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if ($user == false) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Error creating new user account'
            ];

            return response()->json($response, 400);
        }

        $token = $user->createToken($request->device_name)->plainTextToken;

        event(new Registered($user));

        $response = [
            'status' => 'success',
            'message' => 'New User Account Successfully created',
            'token' => $token,
            'user' => $user
        ];

        return response()->json($response, 201);
    }

    public function login(UserLoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            // throw ValidationException::withMessages([
            //     'email' => ['The provided credentials are incorrect.'],
            // ]);

            $response = [
                'status' => 'error',
                'message' => 'The provided credentials are incorrect.'
            ];

            return response()->json($response, 400);
        }

        $token = $user->createToken($request->device_name)->plainTextToken;

        $response = [
            'status' => 'success',
            'message' => 'User account successfully retrieved.',
            'token' => $token,
            'user' => $user
        ];

        return response()->json($response, 200);
    }

    public function logout(Request $request)
    {
        if ($request->user()->currentAccessToken()->delete()) {
            # code...

            $response = [
                'status' => 'success',
                'message' => 'You have been logged out successfully.'
            ];

            return response()->json($response);
        }

        $response = [
            'status' => 'error',
            'message' => 'Error Logging out.'
        ];

        return response()->json($response, 400);
    }

    public function SocialAuthentication($provider)
    {
        if ($provider != 'twitter') {
            # code...

            return Socialite::driver($provider)->stateless()->redirect();
        }
    }

    public function SocialLogin(Request $request, $provider)
    {
        if ($provider == 'google') {
            # code...
            $account = Socialite::driver($provider)->stateless()->user();

            $exists = User::where('email', $account->getEmail())->where('password', '!=', null)->first();

            if ($exists != null) {
                # code...

                $response = [
                    'status' => 'error',
                    'message' => 'This account was manually registered. Kindly login with your email and password'
                ];

                return response()->json($response, 400);
            }

            $data = [
                'email' => $account->getEmail(),
                'first_name' => $account->user['given_name'],
                'last_name' => $account->user['family_name'],
                'provider' => $provider,
                'provider_id' => $account->getId()
            ];

            $user = User::firstOrCreate(
                ['email' => $account->getEmail()],
                $data
            );

            if ($user == false) {
                # code...

                $response = [
                    'status' => 'error',
                    'message' => 'Error creating user account. Kindly try again.'
                ];

                return response()->json($response, 400);
            }

            $token = $user->createToken($request->server('HTTP_USER_AGENT'))->plainTextToken;

            if ($user->wasRecentlyCreated) {
                # code...
                event(new Registered($user));
            }

            $response = [
                'status' => 'success',
                'message' => 'New User Account Successfully Created',
                'token' => $token,
                'user' => $user
            ];

            return response()->json($response, 200);
        }
    }

    public function existingUser(array $data)
    {
        return User::where($data)->exists();
    }

    public function SocialAuth(SocialLoginRequest $request)
    {

        $manual_user = $this->existingUser($request->only('email') + ['password' => !null]);

        if ($manual_user) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'This account was manually registered. Kindly login with your email and password'
            ];

            return response()->json($response, 400);
        };

        $user = User::firstOrCreate(
            ['email' => $request->email],
            $request->except(['device_name', 'provider_id', 'provider'])
        );

        $social_login = SocialLogin::firstOrCreate(
            $request->only(['provider_id', 'provider']),
            $request->only(['provider_id', 'provider']) + ['user_id' => $user->uuid]
        );

        if ($user->wasRecentlyCreated) {
            # code...
            event(new Registered($user));
        }

        $user = $social_login->user;

        $token = $user->createToken($request->device_name)->plainTextToken;

        $response = [
            'status' => 'success',
            'message' => 'User Profile Successfully Retrieved',
            'token' => $token,
            'user' => $user
        ];

        return response()->json($response, 200);
    }

    public function forgot_password(UserForgotPasswordRequest $request)
    {
        Password::sendResetLink($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Reset password Link sent to your email account'
        ];

        return response()->json($response);
    }

    public function reset_password(UserResetPasswordRequest $request)
    {
        $status = Password::reset($request->all(), function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($status == Password::INVALID_TOKEN) {

            return response()->json([
                'status' => 'error',
                'message' => 'Invalid Token Provided'
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Password Successfully Reset'
        ]);
    }

    public function socialAuthProvider(SocialAuthProviderRequest $request)
    {
        $social_login = SocialLogin::with('user')->where('provider_id', $request->provider_id)
            ->where('provider', $request->provider)->first();

        if ($social_login == null) {
            # code...

            return response()->json([
                'status' => 'error',
                'message' => 'Social login provider not found.'
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Social Login info successfully retrieved',
            'data' => new SocialLoginResource($social_login)
        ]);

        // $user = User::where('provider_id', $request->provider_id)->first();

        // if ($user == null) {
        //     # code...

        //     return response()->json([
        //         'status' => 'error',
        //         'message' => 'Invalid Provider Login credentials.'
        //     ], 400);
        // }

        // return response()->json([
        //     'status' => 'success',
        //     'message' => 'User details fetched successfully.',
        //     'data' => $user
        // ]);
    }
}