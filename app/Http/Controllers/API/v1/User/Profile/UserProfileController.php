<?php

namespace App\Http\Controllers\API\v1\User\Profile;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserProfileUpdateRequest;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserProfileUpdateRequest $request, $id)
    {
        //
        $user = User::findBYUUID($id);

        if (is_null($user)) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Requested User Not Found'
            ];

            return response()->json($response);
        }

        $update = $user->update($request->validated());

        if ($update == false) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Error Updating User Profile'
            ];

            return response()->json($response);
        }

        $response = [
            'status' => 'success',
            'message' => 'User Profile Successfully Updated'
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
