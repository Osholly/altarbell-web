<?php

namespace App\Http\Controllers\API\v1\Notification;

use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNewNotification;
use App\Http\Resources\NotificationResource;
use App\Http\Requests\UpdateNotificationRequest;
use App\Http\Resources\NotificationResourceCollection;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewNotification $request)
    {
        //
        $data = [
            'user_id' => $request->user_id,
            'album_id' => $request->album_id,
            'name' => $request->name,
            'time' => $request->time,
            'days' => json_encode($request->days),
            'active' => $request->active,
            'is_repeat' => $request->is_repeat,
            'snooze_time' => $request->snooze_time
        ];

        $notification = Notification::create($data);

        if ($notification == false) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Error creating new notification'
            ];

            return response()->json($response, 400);
        }

        $response = [
            'status' => 'success',
            'message' => 'New Notification successfully created',
            'notification' => new NotificationResource($notification)
        ];

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $notification = Notification::findByUuid($id);

        if ($notification == null) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'requested Notification not found'
            ];

            return response()->json($response, 400);
        }

        return new NotificationResource($notification);
    }

    public function user_notifications($user_id)
    {
        $user = User::where('uuid', $user_id)->with('notifications')->first();

        if ($user == null) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Invalid User ID'
            ];

            return response()->json($response, 400);
        }

        return new NotificationResourceCollection($user->notifications);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNotificationRequest $request, $id)
    {

        $notification = Notification::findByUuid($id);

        if ($notification == null) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Notification not found'
            ];

            return response()->json($response, 400);
        }
        //
        $data = [
            'user_id' => $request->user_id,
            'album_id' => $request->album_id,
            'name' => $request->name,
            'time' => $request->time,
            'days' => json_encode($request->days),
            'active' => $request->active,
            'is_repeat' => $request->is_repeat,
            'snooze_time' => $request->snooze_time
        ];

        $update = $notification->update($data);

        if ($update == false) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Error updating notification'
            ];

            return response()->json($response, 400);
        }

        $response = [
            'status' => 'success',
            'message' => 'Notification successfully updated',
            'notification' => new NotificationResource($notification)
        ];

        return response()->json($response, 200);
    }

    public function delete($id)
    {
        $notification = Notification::findByUuid($id);

        if ($notification == null) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'requested Notification not found'
            ];

            return response()->json($response, 400);
        }

        if ($notification->delete()) {
            # code...

            $response = [
                'status' => 'success',
                'message' => 'Notification successfully deleted'
            ];

            return response()->json($response);
        }

        $response = [
            'status' => 'error',
            'message' => 'Error deleting notification'
        ];

        return response()->json($response, 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
