<?php

namespace App\Http\Controllers\API\v1\Album;

use App\Models\Advert;
use App\Models\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AlbumResource;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\AlbumResourceCollection;

class ApiAlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $albums = Album::has('sounds')->get();

        $adverts = Advert::withoutExpired()->with(['provider', 'user'])->running()->active()->get()->map(function($advert){
            $advert->owner = $advert->user == null ? $advert->provider :$advert->user;
            return $advert;
        });
        $collection = collect();

        foreach ($albums as $album){
            $collection->push($album);
        }

        foreach($adverts as $advert)
        {
            $collection->push($advert);
        }

        $data = $this->customPaginate($collection->shuffle(), 25);
        return new AlbumResourceCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $album = Album::where('uuid', $id)->with(['sounds', 'provider', 'category'])->first();

        if ($album == null) {
            # code...

            $response = [
                'status' => 'error',
                'message' => 'Requested album not found'
            ];

            return response()->json($response, 404);
        }

        $advert = Advert::query()->where('category_id', $album->category_id)->approved()->running()->with(['provider', 'user', 'category']);

        $advert->when($advert->count() > 0, function($query) use($album) {
            return $query->where('category_id', $album->category_id)->running()->active()->orderBy('created_at');
        });

        $advert->when($advert->count() == 0, function($query) {
            return $query->where('status', 1000);               // Target is to return an empty result set.
        });

        $advert = $advert->count() == 0 ? null : collect($advert->get()->map(function ($advert){
            $advert->owner = $advert->user == null ? $advert->provider : $advert->user;
            return $advert;
        })->sortBy('created_at')->random());

        $album = collect($album)->merge(["advert" => $advert]);

        return new AlbumResource($album);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
