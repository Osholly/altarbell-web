<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => ['required', 'string', Rule::unique('albums')->where(function ($query) {
                return $query->where('provider_id', Auth::guard('provider')->user()->uuid)
                    ->where('uuid', !$this->route()->parameters()['id']);
            })],
            'category_id' => 'sometimes|nullable|uuid',
            'album_art' => 'sometimes|nullable|file|image|mimes:png,jpg,jpeg'
        ];
    }
}