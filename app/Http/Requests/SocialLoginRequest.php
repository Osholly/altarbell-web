<?php

namespace App\Http\Requests;

use App\Traits\ResponseTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SocialLoginRequest extends FormRequest
{
    use ResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'provider' => 'required|in:google,facebook,apple|string',
            'provider_id' => 'required|string',
            'first_name' => 'required_unless:provider,apple|string',
            'last_name' => 'required_unless:provider,apple|string',
            'email' => 'required_unless:provider,apple|string',
            'device_name' => 'required|string'
        ];
    }

    /**
     * Throws an exception that shows the error messages
     *
     * @return exception
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->error($validator->errors(), $this->code422));
    }
}
