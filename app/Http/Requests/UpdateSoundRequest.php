<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSoundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'play_date' => ['sometimes', 'nullable', 'date|after:' . Carbon::yesterday(), Rule::unique('sounds')->where(function ($query) {
                return $query->where('album_id', !$this->album_id);
            })],
            'file' => Rule::when(Auth::guard('provider')->id() == 1, 'sometimes|nullable|file', 'sometimes|nullable|file|mimes:mp3,fav,ogg,aac,wma,m4a,flac')
        ];
    }
}
