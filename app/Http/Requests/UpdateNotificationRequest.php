<?php

namespace App\Http\Requests;

use App\Traits\ResponseTrait;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateNotificationRequest extends FormRequest
{
    use ResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'uuid' => 'required|uuid|exists:notifications,uuid',
            'user_id' => 'required|uuid|exists:users,uuid',
            'album_id' => 'required|uuid|exists:albums,uuid',
            'name' => 'required|string|max:255',
            'time' => 'required|date_format:H:i:s',
            'days' => 'required|array',
            'active' => 'required|boolean',
            'is_repeat' => 'required|boolean',
            'snooze_time' => 'required|integer|digits_between:1,60'
        ];
    }


    /**
     * Throws an exception that shows the error messages
     *
     * @return exception
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->error($validator->errors(), $this->code422));
    }
}