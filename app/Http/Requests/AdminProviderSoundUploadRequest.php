<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminProviderSoundUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            //
            'album_id' => 'required|uuid|exists:albums,uuid',
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'play_date' => 'required|date|after:' . today(),
            'file' => 'required|file|mimes:mp3, m4a, flac, wav, aac'
        ];
    }
}