<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class AddSoundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'album_id' => 'required|uuid|exists:albums,uuid',
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'play_date' => 'required|date|after:' . Carbon::yesterday(),
            'file' => Rule::when(Auth::guard('provider')->id() == 1, 'required|file', 'required|file|mimes:mp3,fav,ogg,aac,wma,m4a,flac')
        ];
    }
}