<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProviderProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'country' => 'sometimes|nullable|string',
            'state' => 'sometimes|nullable|string',
            'city' => 'required_with_all:country,state|nullable|string',
            'country_code' => 'required|string',
            'phone' => ['required', 'digits_between:10,11', Rule::unique('providers')->where(function ($query) {
                return $query->where('uuid', '!=', Auth::guard('provider')->user()->uuid);
            })],
            'email' => ['required', 'email', Rule::unique('providers')->where(function ($query) {
                return $query->where('uuid', '!=', Auth::guard('provider')->user()->uuid);
            })],
            'address' => ['required', 'string'],
            'title' => ['required', 'string'],
            'organization' => ['nullable', 'string'],
            'avatar' => Rule::when(!Auth::guard('provider')->user()->avatar, 'file|image|mimes:png,jpg,jpeg|required|max:2000', 'sometimes|nullable|file|image|mimes:png,jpg,jpeg|max:2000')
        ];
    }
}