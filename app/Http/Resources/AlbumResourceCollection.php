<?php

namespace App\Http\Resources;

use App\Http\Resources\PaginationResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AlbumResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $pagination = new PaginationResource($this);

        return [
            'status' => 'success',
            'message' => 'Albums successfully fetched',
            'data' => $this->collection
            // $pagination::$wrap => $pagination,
        ];
    }
}