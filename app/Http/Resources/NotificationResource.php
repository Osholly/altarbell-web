<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'user_id' => $this->user_id,
            'album_id' => $this->album_id,
            'name' => $this->name,
            'time' => Carbon::parse($this->time)->toTimeString(),
            'days' => is_array($this->days) ? $this->days : json_decode($this->days),
            'active' => $this->active,
            'is_repeat' => $this->is_repeat,
            'snooze_time' => $this->snooze_time,
            'created_at' => $this->created_at->toDayDateTimeString(),
            'updated_at' => $this->updated_at->diffForHumans(),
            'album' => $this->album
        ];
    }
}