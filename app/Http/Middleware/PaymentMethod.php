<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PaymentMethod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $payment_method = request()->user('provider')->payment_method;

        if (!$payment_method){
            return redirect()->route('provider.payment-methods.index')->with('error', "Kindly add a payment method to continue");
        }

        return $next($request);
    }
}
