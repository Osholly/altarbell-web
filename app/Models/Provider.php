<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\Album;
use App\Models\Sound;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Qirolab\Laravel\Bannable\Traits\Bannable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\Provider\Auth\VerifyEmail;
use App\Notifications\Provider\Auth\ResetPassword;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Provider extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasUUID, Bannable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone', 'country_code', 'title', 'organization', 'country', 'state', 'city', 'address', 'avatar', 'is_admin', 'admin_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    public function albums()
    {
        return $this->hasMany(Album::class, 'provider_id', 'uuid');
    }

    public function sounds()
    {
        return $this->hasManyThrough(Sound::class, Album::class, 'provider_id', 'album_id', 'uuid', 'uuid');
    }

    public function notifications()
    {
        return $this->hasManyThrough(Notification::class, Album::class, 'provider_id', 'album_id', 'uuid', 'uuid');
    }

    /**
     * Get the admin that owns the Provider
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'uuid');
    }

    public static function search($name)
    {
        return empty($name) ? static::query() :
            static::query()
            ->where(DB::raw('concat("first_name", "last_name")'), 'like', "%{$name}%")
            ->orWhere('first_name', 'like', "%{$name}%")
            ->orWhere('last_name', 'like', "%{$name}%");
    }

    public function payment_method(): HasOne
    {
        return $this->hasOne(PaymentMethod::class, 'provider_id', 'uuid');
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class, 'provider_id', 'uuid');
    }
}
