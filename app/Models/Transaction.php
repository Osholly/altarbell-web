<?php

namespace App\Models;

use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory, HasUUID;

    protected $fillable = [
      'user_id', 'provider_id', 'advert_id', 'amount', 'status', 'description'
    ];

    public function advert(): BelongsTo
    {
        return $this->belongsTo(Advert::class, 'advert_id', 'uuid');
    }
}
