<?php

namespace App\Models;

use Carbon\Carbon;
use FFMpeg\FFProbe;
use App\Models\Album;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sound extends Model
{
    use HasFactory, HasUUID, SoftDeletes;

    protected $fillable = [
        'album_id', 'path', 'play_date', 'title', 'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'play_date' => 'date',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function album()
    {
        return $this->belongsTo(Album::class, 'album_id', 'uuid');
    }

    public function scopeLastMonth($query)
    {
        return $query->whereMonth('play_date', '<=', Carbon::now()->subDays(31)->toDateString());
    }

    public function upcoming()
    {
        return $this->hasOne(Album::class, 'id', 'album_id')
            ->whereDate('play_date', '>=', Carbon::tomorrow()->toDateString());
    }
}