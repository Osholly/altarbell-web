<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Sound;
use App\Models\Provider;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Model;
use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Album extends Model
{
    use HasFactory, HasUUID, SoftDeletes;

    protected $fillable = [
        'provider_id',
        'title',
        'category_id',
        'album_art'
    ];

    protected $with = ['provider', 'sounds'];

    protected $appends = ['category_name'];

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'uuid');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'uuid');
    }

    public function sounds()
    {
        return $this->hasMany(Sound::class, 'album_id', 'uuid')
            ->whereDate('play_date', '>', Carbon::yesterday())
            ->whereDate('play_date', '<=', today()->addDays(2))
            ->orderBy('play_date');
    }

    public function upcoming_sounds()
    {
        return $this->hasMany(Sound::class, 'album_id', 'uuid')
            ->whereDate('play_date', '>', Carbon::yesterday()->toDateString())
            ->orderBy('play_date');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'album_id', 'uuid');
    }

    public function getCategoryNameAttribute()
    {
        return $this->category->name;
    }

    public function next_available_sound_date()
    {
        $latest = Sound::where('album_id', $this->uuid)->orderBy('play_date', 'desc')->first();

        return Carbon::parse($latest->play_date)->addDay(1)->format('Y-m-d');
    }

    public function next_sound_play_date()
    {
        $earliest = $this->upcoming_sounds()->orderBy('play_date')->first()->play_date;

        return Carbon::parse($earliest)->format('Y-m-d');
    }

    public static function search($search)
    {
        return empty($search) ? static::query() :
            static::query()
            ->where('title', 'like', "%{$search}%");
    }

    public function lastMonthSounds()
    {
        return $this->hasMany(Sound::class, 'album_id', 'uuid')->lastMonth();
    }

    public function todaySound($date)
    {
        return $this->hasOne(Sound::class, 'album_id', 'uuid')
            ->where('play_date', $date)->first();
    }
}