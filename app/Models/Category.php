<?php

namespace App\Models;

use App\Models\Album;
use Illuminate\Database\Eloquent\Model;
use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory, HasUUID, SoftDeletes;

    protected $fillable = ['name'];

    public function albums()
    {
        return $this->hasMany(Album::class, 'category_id', 'uuid');
    }
}