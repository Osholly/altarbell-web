<?php

namespace App\Models;

use App\Models\User;
use App\Models\Album;
use App\Models\Sound;
use Illuminate\Database\Eloquent\Model;
use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notification extends Model
{
    use HasFactory, HasUUID;

    protected $with = ['album'];

    protected $fillable = [
        'album_id', 'user_id', 'name', 'time', 'days', 'active', 'is_repeat', 'snooze_time'
    ];

    protected $casts = [
        'days' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'uuid');
    }

    public function album()
    {
        return $this->belongsTo(Album::class, 'album_id', 'uuid');
    }
}