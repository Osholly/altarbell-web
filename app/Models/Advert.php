<?php

namespace App\Models;

use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Mvdnbrk\EloquentExpirable\Expirable;

class Advert extends Model
{
    use HasFactory, HasUUID, Expirable;

    protected $fillable = [
      'category_id', 'title', 'content', 'url', 'image', 'start_date', 'duration', 'status', 'expires_at', 'target', 'views',
        'provider_id'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($advert) {
            $advert->expires_at = Carbon::parse($advert->start_date)->addDays($advert->duration)->diffInSeconds(now());
            $advert->save();
        });
    }

    public function scopePending($query)
    {
        return $query->where('status', null);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'uuid');
    }

    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'uuid');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'uuid');
    }

    public function owner()
    {
        if ($this->user()->doesntExist()){
            return $this->belongsTo(Provider::class, 'provider_id', 'uuid');
        }

        if ($this->provider()->doesntExist()){
            return $this->belongsTo(User::class, 'user_id', 'uuid');
        }
    }

    public function getEndDateAttribute()
    {
        return Carbon::parse($this->start_date)->addDays($this->duration)->toDateString();
    }

    public function scopeRunning($query)
    {
        return $this->whereRaw("target > views");
    }

    public function scopeCompleted($query)
    {
        return $query->whereRaw("target <= views");
    }

    public function scopeApproved($query)
    {
        return $query->where('status', 1);
    }

    public function transaction(): HasOne
    {
        return $this->hasOne(Transaction::class, 'advert_id', 'uuid');
    }
}
