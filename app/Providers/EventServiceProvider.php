<?php

namespace App\Providers;

use App\Events\AdvertApprovedEvent;
use App\Events\AdvertPaymentFailureEvent;
use App\Events\AdvertPaymentSuccessfulEvent;
use App\Events\PaymentMethodUpdatedEvent;
use App\Listeners\ChargeProviderListener;
use App\Listeners\PendingAdvertChargeListener;
use App\Listeners\SendPaymentFailureNotification;
use App\Listeners\SendPaymentSuccessNotification;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\CreateDefaultNotifications;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            CreateDefaultNotifications::class,
        ],

        AdvertApprovedEvent::class => [
          ChargeProviderListener::class,
        ],

        AdvertPaymentSuccessfulEvent::class => [
          SendPaymentSuccessNotification::class,
        ],

        AdvertPaymentFailureEvent::class => [
          SendPaymentFailureNotification::class,
        ],

        PaymentMethodUpdatedEvent::class => [
          PendingAdvertChargeListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
