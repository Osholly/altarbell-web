<?php

namespace App\Events;

use App\Models\Advert;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AdvertApprovedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $advert;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Advert $advert)
    {
        $this->advert = $advert;
    }

}
