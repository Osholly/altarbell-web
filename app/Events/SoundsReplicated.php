<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SoundsReplicated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $album;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($album)
    {
        //
        $this->album = $album;
    }
}